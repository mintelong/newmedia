package com.rechaos.service

import com.rechaos.analysis.Article
import com.rechaos.analysis.Wordcount
import com.rechaos.lingpipe.AnsjTerm
import com.rechaos.lingpipe.CopyOfRunMedPost
import com.rechaos.lingpipe.CopyOfPolarityBasic

import java.util.regex.Matcher
import java.util.regex.Pattern

class DataAnalysis {
	
	public final static String STATS_AnalysisED = "sign"
	
	public void title(String rootPath,String cate){
		
		List<String> words = this.readFileByLines(rootPath+"models\\stopwords\\StopWords.txt")
		
		//List<String> words = this.readFileByLines(rootPath+"models/stopwords/StopWords.txt")
		
		List<Article> articles = Article.findAll()
		StringBuffer strb = new StringBuffer()
		AnsjTerm at = new AnsjTerm();
		for(Article article : articles){
			
			if(!article.tAnalysisStats.equals(STATS_AnalysisED)){
				if("title".equals(cate)){
					strb.append(article.getTitle())
					article.tAnalysisStats = STATS_AnalysisED
				}
			}
			if(!article.cAnalysisStats.equals(STATS_AnalysisED)){
				if("content".equals(cate)){
					strb.append(article.getContent())
					article.cAnalysisStats = STATS_AnalysisED
				}
			}
			article.save(flush: true)
		}
//		println at.StringSplit(strb.toString()).split(" ")
//		List<String> strs = new ArrayList<String>();
//		Collections.addAll(strs, at.StringSplit(strb.toString()).split(" "))
		
		//特殊数字替换
		String strContent = strb.toString()
		String replacePattern = "(\\d+)年|(\\d+)月|(\\d+)日|(\\d+)时|(\\d+)分|(\\d+)秒|(\\d+)号|(\\d+)后|(\\d+)D|(\\d+)天";
		Pattern re = Pattern.compile(replacePattern);
		Matcher me = re.matcher(strContent);	
		while (me.find()) {
			String keye = me.group(0);
		//	System.out.println(keye);
			strContent = strContent.replaceAll(keye, "");
		}
		
		List<String> strs = CopyOfRunMedPost.tag("",strContent)
		Map <String,Integer> map = new HashMap <String,Integer>();
		for(Object o :strs) {
			map.put(o, map.get(o)==null?1:map.get(o)+1);
		}
		
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
			String trueword = entry.getKey().split("_")[0]
			String key = trueword.replaceAll("\\pP", "~")
			if(!key.contains("~")&&!trueword.trim().equals("")&&this.content(trueword)&&!words.contains(trueword)){
				Wordcount word = new Wordcount()
				word.word = entry.getKey().split("_")[0]
				word.count = entry.getValue()
				word.polarity = new CopyOfPolarityBasic(word.word).run();
				word.cate = cate
				word.cix = entry.getKey().split("_")[1]
//				println "123jianjiandan"+word.cix
				Save(word)
				
			}
		}
	}
	
	private Save(Wordcount word){
		//find word recode
		def words = Wordcount.findAllByWord(word.word)
		def findFlag = false;
		
		for(Wordcount oneWord: words){
			if(oneWord.equals(word)){
				//combine and save
				findFlag = true;
				oneWord.setCount((int)oneWord.count+(int)word.count)
				println "===="+oneWord.word+"====wordCount===update======"+oneWord.count
				oneWord.save(flush:true)
			}
		}
		//save
		if(!findFlag){
			word.save(flush:true)
		}
		
	}
	//词性转换中文
	public String cix(String cix){
		String re="其它";
		if(cix.startsWith("a")){
			re = "形容词"
		}
		if(cix.startsWith("n")){
			re = "名词"
		}
		if(cix.startsWith("v")){
			re = "动词"
		}
		return re
	}
	public boolean content(String word){
		List<String> words = new ArrayList<String>()
		words.add("的")
		words.add("了")
		words.add("你")
		words.add("有")
		words.add("看")
		words.add("就")
		words.add("真")
		words.add("要")
		words.add("般")
		words.add("是")
		words.add("长")
		
		
		if(words.contains(word)){
			return false
		}
		return true
	}
	/**
	 * 以行为单位读取文件，常用于读面向行的格式化文件
	 */
	public static List<String> readFileByLines(String fileName) {
		File file = new File(fileName);
		BufferedReader reader = null;
		List<String> words = new ArrayList<String>()
		
		try {
			System.out.println("以行为单位读取文件内容，一次读一整行：");
			reader = new BufferedReader(new FileReader(file));
			String tempString = null;
			int line = 1;
			// 一次读入一行，直到读入null为文件结束
			while ((tempString = reader.readLine()) != null) {
				// 显示行号
				System.out.println("line " + line + ": " + tempString);
				words.add(tempString)
				line++;
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
				}
			}
		}
		return words
	}
	public static void main(String[] args){
		this.title("")
	}
}

