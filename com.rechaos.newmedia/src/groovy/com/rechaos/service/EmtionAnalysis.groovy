package com.rechaos.service

import com.rechaos.AnalysisPlugin.ContextAnalysis
import com.rechaos.analysis.Article
import com.rechaos.analysis.ArticleEmtion

class EmtionAnalysis {
	public final static String STATS_AnalysisED = "sign"
	public final static String Type_anger = "anger"
	public final static String Type_anxiety = "anxiety"
	public final static String Type_positive = "positive"
	public final static String Type_sadness = "sadness"
	public final static String Type_hate = "hate"
	public final static String Type_happy = "happy"
	public final static String Type_surprise = "surprise"
	
	public static void getArticleEmtion(String rootPath){
		List<Article> articles = Article.findAll()
		for(Article article : articles){
			
			if(!article.eAnalysisStats.equals(STATS_AnalysisED)){
				//run analysis
				ContextAnalysis cAnalysis = new ContextAnalysis(article.content, rootPath)
				SaveToDB("愤怒", Type_anger, cAnalysis.getAngerRank(), article.wechats)
				SaveToDB("焦虑", Type_anxiety, cAnalysis.getAnxietyRank(), article.wechats)
				SaveToDB("肯定", Type_positive, cAnalysis.getPositiveEmtionRank(), article.wechats)
				SaveToDB("悲伤", Type_sadness, cAnalysis.getSadnessRank(), article.wechats)
				SaveToDB("厌恶", Type_hate, cAnalysis.getHateRank(), article.wechats)
				SaveToDB("高兴", Type_happy, cAnalysis.getHappyRank(), article.wechats)
//				ArticleEmtion 
				article.eAnalysisStats = STATS_AnalysisED
				article.save(flush : true)
			}
		}
	}
	
	private static void SaveToDB(String name, String type, double value, int shareCount){
		ArticleEmtion artEm = ArticleEmtion.findByEmtionType(type);
		if(null == artEm){
			artEm = new ArticleEmtion()
			artEm.emtionName = name
			artEm.emtionType = type
			artEm.artcleCount = 1
			artEm.shareCount  = shareCount
			artEm.save(flush: true)
		}
		else{
			if(!(value - 0.0 <= Double.MIN_VALUE)){
//				artEm.emtionName = name
				artEm.artcleCount = ((int)artEm.artcleCount)+1
				artEm.shareCount  = ((int)artEm.shareCount) +shareCount
				artEm.save(flush: true)
			}
		}
		
	}
}
