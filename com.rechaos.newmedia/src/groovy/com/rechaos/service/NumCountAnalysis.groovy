package com.rechaos.service
import com.rechaos.analysis.*;

import java.util.Map;
import java.util.Map.Entry
import java.util.regex.Matcher
import java.util.regex.Pattern

class NumCountAnalysis {

	public static String Type_Article_Num = "Type_Article_Num";
	public static String Type_TView_Count = "Type_TView_Count";
	public static String Type_TShare_Count = "Type_TShare_Count";
	public static String Type_CView_Count = "Type_CView_Count";
	public static String Type_CShare_Count = "Type_CShare_Count";
	
	public static String Type_View_Share = "Type_View_Share";
	
	public static Map<String, Integer> getNumCount(){
		def allAriticle = Article.findAll();
		Map<String, Integer> result =  new HashMap<String, Integer>();
		for(Article atc: allAriticle){
			Map<String, Integer> tCount = numCount(atc.getTitle())
//			Map<String, Integer> cCount = numCount(atc.getContent())
			
			//合并
			for(Entry<String, Integer> entry: tCount.entrySet()){
				if (null== result.get(entry.getKey())){
					result.put(entry.getKey(), entry.getValue())
				}
				else{
					result.put(entry.getKey(), entry.getValue()+result.get(entry.getKey()))
				}
			}
		}
		result
	}
	
	public static Map<String, Integer> getCViewCount(){
		def allAriticle = Article.findAll();
		Map<String, Integer> result =  new HashMap<String, Integer>();
		for(Article atc: allAriticle){
//			Map<String, Integer> tCount = numCount(atc.getTitle())
			Map<String, Integer> cCount = numCountContentLength(atc.getContent())
			
			//合并
			for(Entry<String, Integer> entry: cCount.entrySet()){
				if (null== result.get(entry.getKey())){
					result.put(entry.getKey(), atc.getPageviews())
				}
				else{
					result.put(entry.getKey(), atc.getPageviews()+result.get(entry.getKey()))
				}
			}
		}
		result
	}
	public static Map<String, Integer> getCShareCount(){
		def allAriticle = Article.findAll();
		Map<String, Integer> result =  new HashMap<String, Integer>();
		for(Article atc: allAriticle){
//			Map<String, Integer> tCount = numCount(atc.getTitle())
			Map<String, Integer> cCount = numCountContentLength(atc.getContent())
			
			//合并
			for(Entry<String, Integer> entry: cCount.entrySet()){
				if (null== result.get(entry.getKey())){
					result.put(entry.getKey(), atc.getWechats())
				}
				else{
					result.put(entry.getKey(), atc.getWechats()+result.get(entry.getKey()))
				}
			}
		}
		result
	}
	
	public static Map<String, Integer> getTViewCount(){
		def allAriticle = Article.findAll();
		Map<String, Integer> result =  new HashMap<String, Integer>();
		for(Article atc: allAriticle){
			Map<String, Integer> tCount = numCount(atc.getTitle())
//			Map<String, Integer> cCount = numCount(atc.getContent())
			
			//合并
			for(Entry<String, Integer> entry: tCount.entrySet()){
				if (null== result.get(entry.getKey())){
					result.put(entry.getKey(), atc.getPageviews())
				}
				else{
					result.put(entry.getKey(), atc.getPageviews()+result.get(entry.getKey()))
				}
			}
		}
		result
	}
	public static Map<String, Integer> getTShareCount(){
		def allAriticle = Article.findAll();
		Map<String, Integer> result =  new HashMap<String, Integer>();
		for(Article atc: allAriticle){
			Map<String, Integer> tCount = numCount(atc.getTitle())
//			Map<String, Integer> cCount = numCount(atc.getContent())
			
			//合并
			for(Entry<String, Integer> entry: tCount.entrySet()){
				if (null== result.get(entry.getKey())){
					result.put(entry.getKey(), atc.getWechats())
				}
				else{
					result.put(entry.getKey(), atc.getWechats()+result.get(entry.getKey()))
				}
			}
		}
		result
	}
	
	public static Map<String, Integer> getVShareCount(){
		def allAriticle = Article.findAll();
		Map<String, Integer> result =  new HashMap<String, Integer>();
		for(Article atc: allAriticle){
			Map<String, Integer> tCount = new HashMap<String, Integer>();
			tCount.put(atc.getPageviews().toString(), atc.getWechats())
//			Map<String, Integer> cCount = numCount(atc.getContent())
			//合并
			for(Entry<String, Integer> entry: tCount.entrySet()){
				if (null== result.get(entry.getKey())){
					result.put(entry.getKey(), atc.getWechats())
				}
				else{
					result.put(entry.getKey(), atc.getWechats()+result.get(entry.getKey()))
				}
			}
		}
		result
	}
	
	//计算文章长度
	public  static Map<String, Integer> numCountContentLength(String content){
		Map<String, Integer> result =  new HashMap<String, Integer>();
		
		result.put(content.length().toString(), 1)
		
		result
	}
	
	public  static Map<String, Integer> numCount(String content){
		Map<String, Integer> result =  new HashMap<String, Integer>();
		
		//特殊数字替换
		String replacePattern = "(\\d+)年|(\\d+)月|(\\d+)日|(\\d+)时|(\\d+)分|(\\d+)秒|(\\d+)号|(\\d+)后|(\\d+)D|(\\d+)天|(\\d+)时刻";
		Pattern re = Pattern.compile(replacePattern);
		Matcher me = re.matcher(content);
		while (me.find()) {
			String keye = me.group(0);
		//	System.out.println(keye);
			content = content.replaceAll(keye, "");
		}
		
		String pattern = "(\\d+)"; 
		// 创建 Pattern 对象
		Pattern r = Pattern.compile(pattern);
  
		// 现在创建 matcher 对象
		Matcher m = r.matcher(content);
//		int count = 0;
		while (m.find()) {
//			println m.groupCount();
			String key = m.group(0)
			println("Found num: " + m.group(0));
//		    count++;
			if (null== result.get(key)){
				result.put(key, 1)
			}
			else{
				result.put(key, 1+result.get(key))
			}

		}
		result
	}
	
	public static void ConvertAndSave(String name, Map<String, Integer> data){
		ArticleNumRpt atc = ArticleNumRpt.findByReportName(name);
		
		if(null == atc){
			atc = new ArticleNumRpt();
			atc.setReportName(name);
		}
		Map<Integer, Integer> result = convert(data);
		atc.setReport(data);
		atc.save (flush: true)
	}
	
	public static Map<Integer,Integer> convert(Map<String, Integer> data){
		Map<Integer,Integer> result = new HashMap<Integer,Integer>();
		for(Entry<String, Integer> entry: data.entrySet()){
			result.put(Integer.parseInt(entry.getKey()), entry.getValue());
		}
		return result
	}
	
	
	public static Map<Integer, Integer> combine(Map<Integer, Integer> input, int step = 100){
		//排序和分组
		if(input == null){
			return input;
		}
		
		def newlist = new HashMap<Integer, Integer>();
		int num = step;
		int i = 0;
		for(def elment :input){
			num = ((int)(elment.key/step) + 1)*step
				//合并map
			if (null== newlist.get(num)){
				newlist.put(num, elment.getValue())
			}
			else{
				newlist.put(num, elment.getValue()+newlist.get(num))
			}
//				num+= step;
		}
		return newlist;
	}
	
	public static void main(String[] args){
//		String str ="哈迪斯佛123方家安当老123师空间发了,我43什么33鬼神鬼3378浮点数"
//		println NumCountAnalysis.numCount(str)
		println NumCountAnalysis.getNumCount()
	}
}
