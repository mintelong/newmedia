package com.rechaos.AnalysisPlugin;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;

import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.ToAnalysis;
import org.ansj.util.FilterModifWord;

import com.rechaos.lingpipe.AnsjTokenizerFactory;
import com.rechaos.service.DataAnalysis;
import com.rechaos.util.FileUtil;

/**
 * @author hankcs
 */
public class TextRankKeyword
{
    private int nKeyword = 10;
    private final float d = 0.85f;
    /**
     * 最大迭代次数
     */
    private final int max_iter = 200;
    private final float min_diff = 0.001f;

    public TextRankKeyword()
    {
        // jdk bug : Exception in thread "main" java.lang.IllegalArgumentException: Comparison method violates its general contract!
        System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
    }
    
    public TextRankKeyword(int nKeyword)
    {
    	this.nKeyword = nKeyword;
    }

    public List<Map.Entry<String, Float>> getKeyword(String rootPath, String content)
    {
    	AnsjTokenizerFactory ansj = new AnsjTokenizerFactory();
		// 加入停用词
//		FilterModifWord.insertStopWords(ansj
//				.addStopwords("/newWord/newWordFilter.dic"));
    	//首先对这句话分词，这里可以借助各种分词项目，比如Ansj分词，得出分词结果：
    	List<String> stopWords = DataAnalysis.readFileByLines(rootPath+"models/stopwords/StopWords.txt");
        List<Term> termList = ToAnalysis.parse(rootPath + content);
        // 修正词性并且过滤停用
        termList = FilterModifWord.modifResult(termList);
        //然后去掉里面的停用词，这里我去掉了标点符号、常用词、以及“名词、动词、形容词、副词之外的词”。得出实际有用的词语：
        List<String> wordList = new ArrayList<String>();
        for (Term t : termList)
        {
            if (shouldInclude(t)&& !stopWords.contains(t.getName()))
            {
                wordList.add(t.getName());
            }
        }
//        System.out.println(wordList);
        //之后建立两个大小为5的窗口，每个单词将票投给它身前身后距离5以内的单词：
        System.out.println(wordList);
        
        
        Map<String, Set<String>> words = new HashMap<String, Set<String>>();
        Queue<String> que = new LinkedList<String>();
        for (String w : wordList)
        {
            if (!words.containsKey(w))
            {
                words.put(w, new HashSet<String>());
            }
            //Inserts the specified element into this queue if it is possible to do so immediately without violating capacity restrictions.
            que.offer(w);
            if (que.size() > 5)
            {
            	//Retrieves and removes the head of this queue, or returns null if this queue is empty.
                que.poll();
            }

            for (String w1 : que)
            {
                for (String w2 : que)
                {
                    if (w1.equals(w2))
                    {
                        continue;
                    }

                    words.get(w1).add(w2);
                    words.get(w2).add(w1);
                }
            }
        }
        //然后开始迭代投票：
//        System.out.println(words);
        Map<String, Float> score = new HashMap<String, Float>();
        for (int i = 0; i < max_iter; ++i)
        {
            Map<String, Float> m = new HashMap<String, Float>();
            float max_diff = 0;
            for (Map.Entry<String, Set<String>> entry : words.entrySet())
            {
                String key = entry.getKey();
                Set<String> value = entry.getValue();
                m.put(key, 1 - d);
                for (String other : value)
                {
                    int size = words.get(other).size();
                    if (key.equals(other) || size == 0) continue;
                    m.put(key, m.get(key) + d / size * (score.get(other) == null ? 0 : score.get(other)));
                }
                max_diff = Math.max(max_diff, Math.abs(m.get(key) - (score.get(key) == null ? 0 : score.get(key))));
            }
            score = m;
            if (max_diff <= min_diff) break;
        }
        List<Map.Entry<String, Float>> entryList = new ArrayList<Map.Entry<String, Float>>(score.entrySet());
        Collections.sort(entryList, new Comparator<Map.Entry<String, Float>>()
        {
            @Override
            public int compare(Map.Entry<String, Float> o1, Map.Entry<String, Float> o2)
            {
                return (o1.getValue() - o2.getValue() > 0 ? -1 : 1);
            }
        });
//        System.out.println(entryList);
//        String result = "";
        List<Map.Entry<String, Float>> result = new ArrayList<Map.Entry<String, Float>>(nKeyword);
        for (int i = 0; i < nKeyword&i<entryList.size(); ++i)
        {
        	result.add(entryList.get(i));
//            result += entryList.get(i).getKey() + "=" + entryList.get(i).getValue() + ", ";
        }
//        result = result.substring(0,result.length() - 2);
//        return result +="";
//        System.out.println(result);
        return result;
    }

    public static void main(String[] args)
    {
    	
    	TextRankKeyword rankword = new TextRankKeyword(30);
		List<Entry<String, Float>> result = rankword.getKeyword("", "自带自拍杆，不占空间，便于携带。\r\n\r\n放开那个手机，让我来~！\r\n\r\n2 懒是一种境界\r\n\r\n3 现在的卖家啊。。。这效果也是醉了\r\n\r\n4 老爷子好牛逼\r\n\r\n5 今天加油无意看到的！老板你是有多爱麻将？\r\n\r\n6 朋友说不能多喝，就一杯意思意思\r\n\r\n7 大叔您可坐稳了，接下来感受“速度与激情”。\r\n\r\n8 韩国学生过起愚人节来还蛮认真的……老师也是配合\r\n\r\n9 熊孩纸谁告诉你这个可以戴头上的！\r\n\r\n10 插上翅膀的电动车，想要飞却怎么也飞不高。\r\n\r\n11 看着都蛋疼……\r\n\r\n12 卖包子勒，热腾腾，刚出炉的包子勒\r\n\r\n13 难道就没有人愿意给你个筐吗？\r\n\r\n14 来来来，筐要多少自己拿，别跟我客气。\r\n\r\n15 亮点在哪里？注意观察司机大哥的脑袋\r\n\r\n16 抬头望一望树梢，就知道春天来了");
//		for(Entry<String,Float> e : result){
//			System.out.println("\t"+e);
//		}
    }

    class Document
    {
        Map<String, Set<String>> words;
        Map<String, Float> score;
    }

    /**
     * 是否应当将这个term纳入计算，词性属于名词、动词、副词、形容词
     * @param term
     * @return 是否应当
     */
    public boolean shouldInclude(Term term)
    {
//    	System.out.print(term.getName() + "\t");
        if (
                term.getNatrue().natureStr.startsWith("n") ||
                term.getNatrue().natureStr.startsWith("v") ||
                term.getNatrue().natureStr.startsWith("d") ||
                term.getNatrue().natureStr.startsWith("a")
            )
        {
            // TODO 你需要自己实现一个停用词表
            if (!StopWordDictionary.contains(term.getName()))
            {
                return true;
            }
        }

        return false;
    }
 
}

class StopWordDictionary{
	static public boolean contains(String name){
		if(name.equalsIgnoreCase(" ")) return true;
		else return false;
	}
}
