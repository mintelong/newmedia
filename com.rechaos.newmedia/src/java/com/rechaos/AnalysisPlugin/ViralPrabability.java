package com.rechaos.AnalysisPlugin;

public class ViralPrabability {
	
	
	static double Positive_Contribution_Rate = 0.23;
	static double Emotionality_Contribution_Rate = 0.29*0.15;
//	static double Awe_Contribution_Rate = 0.0;
//	static double Anger_Contribution_Rate = 0.0;
//	static double Anxiety_Contribution_Rate = 0.0;
//	static double Sadness_Contribution_Rate = -0.0;
	static double Awe_Contribution_Rate = 0.36;
	static double Anger_Contribution_Rate = 0.37;
	static double Anxiety_Contribution_Rate = 0.27;
	static double Sadness_Contribution_Rate = -0.16;
	static double BASE_PROBABILITY = Positive_Contribution_Rate
			+Emotionality_Contribution_Rate
			+Awe_Contribution_Rate
			+Anger_Contribution_Rate
			+Anxiety_Contribution_Rate;

	private double Positive_Rank;
	private double Emotionality_Rank;
	private double Awe_Rank;
	private double Sadness_Rank;
	private double Anxiety_Rank;
	private double Anger_Rank;

	private ContextAnalysis context;
	
	public ViralPrabability() {
	}
	
	public ViralPrabability(ContextAnalysis context){
		this.context = context;
		setAnger_Rank(context.getAngerRank());
		setAnxiety_Rank(context.getAnxietyRank());
//		setAwe_Rank(context.getAweRank());
//		setEmotionality_Rank(context.getEmotionalityRank());
		setPositive_Rank(context.getPositiveRank());
		setSadness_Rank(context.getSadnessRank());
	}
	
	public static void setBASE_PROBABILITY(double bASE_PROBABILITY) {
		BASE_PROBABILITY = bASE_PROBABILITY;
	}

	public static void setPositive_Contribution_Rate(
			double positive_Contribution_Rate) {
		Positive_Contribution_Rate = positive_Contribution_Rate;
	}

	public static void setEmotionality_Contribution_Rate(
			double emotionality_Contribution_Rate) {
		Emotionality_Contribution_Rate = emotionality_Contribution_Rate;
	}

	public static void setAwe_Contribution_Rate(double awe_Contribution_Rate) {
		Awe_Contribution_Rate = awe_Contribution_Rate;
	}

	public static void setAnger_Contribution_Rate(double anger_Contribution_Rate) {
		Anger_Contribution_Rate = anger_Contribution_Rate;
	}

	public static void setAnxiety_Contribution_Rate(double anxiety_Contribution_Rate) {
		Anxiety_Contribution_Rate = anxiety_Contribution_Rate;
	}

	public static void setSadness_Contribution_Rate(double sadness_Contribution_Rate) {
		Sadness_Contribution_Rate = sadness_Contribution_Rate;
	}

	public double getPositive_Rank() {
		return Positive_Rank;
	}

	public void setPositive_Rank(double positive_Rank) {
		Positive_Rank = positive_Rank;
	}

	public double getEmotionality_Rank() {
		return Emotionality_Rank;
	}

	public void setEmotionality_Rank(double emotionality_Rank) {
		Emotionality_Rank = emotionality_Rank;
	}

	public double getAwe_Rank() {
		return Awe_Rank;
	}

	public void setAwe_Rank(double awe_Rank) {
		Awe_Rank = awe_Rank;
	}

	public double getSadness_Rank() {
		return Sadness_Rank;
	}

	public void setSadness_Rank(double sadness_Rank) {
		Sadness_Rank = sadness_Rank;
	}

	public double getAnxiety_Rank() {
		return Anxiety_Rank;
	}

	public void setAnxiety_Rank(double anxiety_Rank) {
		Anxiety_Rank = anxiety_Rank;
	}

	public double getAnger_Rank() {
		return Anger_Rank;
	}

	public void setAnger_Rank(double anger_Rank) {
		Anger_Rank = anger_Rank;
	}

	public ContextAnalysis getContext() {
		return context;
	}

	public void setContext(ContextAnalysis context) {
		this.context = context;
	}

	public double getViralPrabability(){
		return 100*Math.log((getEffectFactor()/BASE_PROBABILITY)*(Math.E-1.0)+1);
	}

	private double getEffectFactor() {
		
		return Positive_Rank * Positive_Contribution_Rate 
				+ Emotionality_Rank * Emotionality_Contribution_Rate 
				+ Awe_Rank * Awe_Contribution_Rate 
				+ Sadness_Rank * Sadness_Contribution_Rate
				+ Anxiety_Rank * Anxiety_Contribution_Rate
				+ Anger_Rank * Anger_Contribution_Rate;
	}
	
	public static void main(String [] args){
		String context = null;
//		ViralPrabability t = new ViralPrabability(new ContextAnalysis(context));
//		t.getViralPrabability();
	}
	
}
