package com.rechaos.AnalysisPlugin;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DicUtil {
	
	@SuppressWarnings("resource")
	public static Map<String, EmtionEntry> LoadDicByExcel(String path){
		  // 构造 XSSFWorkbook 对象，strPath 传入文件路径
	    XSSFWorkbook xwb = null;
		try {
			xwb = new XSSFWorkbook("web-app/dic/情感词汇本体.xlsx");
		//	xwb = new XSSFWorkbook("/usr/lib/tools/tomcat/apache-tomcat-7.0.52/webapps/newmedia/dic/情感词汇本体.xlsx");
		} catch (IOException e) {
			e.printStackTrace();
		}
	    // 读取第一章表格内容
	    XSSFSheet sheet = xwb.getSheetAt(0);
	    // 定义 row、cell
	    XSSFRow row;
	    String cell;
	    // 循环输出表格中的内容
	    Map<String, EmtionEntry> dic = new HashMap<String, EmtionEntry>();
	    for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
	        row = sheet.getRow(i);
	        EmtionEntry entry = new EmtionEntry();
	        for (int j = row.getFirstCellNum(); j < row.getPhysicalNumberOfCells(); j++) {
	            // 通过 row.getCell(j).toString() 获取单元格内容，
	        	if(row.getCell(j) == null){
	        		continue;
	            }
	        	cell = row.getCell(j).toString();
	        	if(j == 0){
	        		entry.setWord(cell);
	        	}else if (j == 4){
	        	    entry.setType(cell);
	        	}else if (j == 5){
	        		entry.setStrength(Double.parseDouble(cell));
	        	}else if (j == 6){
	        		entry.setPolartiy(cell);
	        	}
	        }
	        dic.put(entry.getWord(),entry);
	    }
	    return dic;
	} 
  
}
