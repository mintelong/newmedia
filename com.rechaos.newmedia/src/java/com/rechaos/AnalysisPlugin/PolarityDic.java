package com.rechaos.AnalysisPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rechaos.util.FileUtil;

public class PolarityDic {
	
	private Map<String,EmtionEntry> dic;
	private String path;
	private List<String>category;
	private File rootDir;
	
	public PolarityDic(){
		category = new ArrayList<String>();
		dic = new HashMap<String, EmtionEntry>();
	}
	
	public PolarityDic(String fileDir){
		category = new ArrayList<String>();
		dic = new HashMap<String, EmtionEntry>();
		this.path = fileDir;
		loadDic(this.path);
	}
	
	
	public void loadDic(String path){
		rootDir = new File(path);
		File[] root = rootDir.listFiles();
		for(File catalog: root){
			String polarity = catalog.getName();
			category.add(polarity);
			File []files = catalog.listFiles();
			for(File file: files){
				List<String> result = null;
				try {
					result = FileUtil.reading(file.getAbsolutePath(),"gbk");
				} catch (IOException e) {
					e.printStackTrace();
				}
				for(String word: result){
					EmtionEntry entry = new EmtionEntry();
					entry.setPolartiy(polarity);
					dic.put(word, entry);
				}
			}
		}
	}
	
	public EmtionEntry getWordPolarity(String word){
		return dic.get(word);
	}
	
	public Map<String, EmtionEntry> getDic() {
		return dic;
	}

	public void setDic(Map<String, EmtionEntry> dic) {
		this.dic = dic;
	}

	
	public List<String> getCategory() {
		return category;
	}

	public void setCategory(List<String> category) {
		this.category = category;
	}

	public static void main(String [] args){
		PolarityDic dic = new PolarityDic();
		dic.setDic(DicUtil.LoadDicByExcel(""));
		try {
			System.out.println(dic.getWordPolarity("美好"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	} 
}
