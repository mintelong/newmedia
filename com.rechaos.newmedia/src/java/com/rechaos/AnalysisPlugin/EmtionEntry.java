package com.rechaos.AnalysisPlugin;

public class EmtionEntry {

	private String type;
	private String word;
	private int strength;
	private String polartiy;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public int getStrength() {
		return strength;
	}
	public void setStrength(int strength) {
		this.strength = strength;
	}
	public void setStrength(double strength) {
		this.strength = (int) strength;
	}
	public String getPolartiy() {
		return polartiy;
	}
	public void setPolartiy(String polartiy) {
		if(polartiy.equals("1.0")){
			this.polartiy = "pos";
		}else if(polartiy.equals("0.0")){
			this.polartiy = "neu";
		}else if(polartiy.equals("2.0")){
			this.polartiy = "neg";
		}else{
			this.polartiy = "neu";
		}
	}
	@Override
	public String toString() {
		return this.word + "\t"+this.polartiy+ "\t"+ 
				this.strength + "\t"+this.type;
	}

	
	
}
