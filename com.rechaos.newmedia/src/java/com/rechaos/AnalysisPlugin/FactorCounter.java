package com.rechaos.AnalysisPlugin;

public class FactorCounter {

	private double polarity = 0.0;
	
	private double anger = 0.0;    //愤怒
	private double anxiety = 0.0;  //焦虑
	private double positive = 0.0; //肯定
	private double sadness = 0.0;  //悲伤
	private double hate = 0.0;     //厌恶
	private double happy = 0.0; //高兴
	private double surprise = 0.0;  //惊讶
	
	


	/***分享量高通用的指标****/
	private int clicknum = 0;
	private int sharingnum = 0;
	private int apprisenum = 0;
	
	public static double PolarityValue(String key){
		if(key != null)
			if(key.equals("pos")){
				return  1.0;
			}else if(key.equals("neg")){
				return  -1.0;
			}else{
				return 0.0;
				
			}
		return 0.0;
	}
	

	public void PositiveValue(String key, int strength, float weight) {
		if(key != null)
			if(key.equals("PD")||key.equals("PH")||key.equals("PG")||key.equals("PB")||key.equals("PK")){
				positive += (1.0*strength*weight);
			}
			else{
				//add(0.0);
			}
	}
	
	public void PolarityValue(String key, int strength, float weight) {
		if(key != null)
			if(key.equals("pos")){
				polarity += (1.0*strength*weight);
			}else if(key.equals("neg")){
				polarity += (-1.0*strength*weight);
			}else{
				//add(0.0);
			}
	}
	
	public void HappinessValue(String key, int strength, float weight) {
		if(key != null)
			if(key.equals("PA")||key.equals("PE")){
				happy += (1.0*strength*weight);
			}
			else{
				//add(0.0);
			}
	}
	
	public void HateValue(String key, int strength, float weight){
		if(key != null)
			if(key.equals("NE")
					||key.equals("NL")||key.equals("NI")
					||key.equals("NC")||key.equals("NG")){
				hate += (1.0*strength*weight);
			}else{
				//return 0.0;
			}
		//return 0.0;
	}
	
	public void AngerValue(String key, int strength, float weight){
		if(key != null)
			if(key.equals("NA")||key.equals("ND")){
				anger += (1.0*strength*weight);
			}else{
//				return 0.0;
			}
//		return 0.0;
	}
	
//	public void AweValue(String key, int strength, float weight){
//		if(key != null)
//			if(key.equals("PD")||key.equals("PG")){
//				awe += (1.0*strength*weight);
//			}else{
////				return 0.0;
//			}
////		return 0.0;
//	}
	
	public void SadnessValue(String key, int strength, float weight){
		if(key != null)
			if(key.equals("NH")||key.equals("NJ")||key.equals("NB")||key.equals("PF")){
				sadness += (1.0*strength*weight);
			}else{
//				return 0.0;
			}
//		return 0.0;
	}
	
	public  void AnxietyValue(String key, int strength, float weight){
		if(key != null)
			if(key.equals("NI")||key.equals("NC")||key.equals("NG")){
//				return 0.0;
			}else{
				anxiety +=(1.0*strength*weight);
			}
//		return 0.0;
	}

	public double getPolarity() {
		return polarity;
	}


	public void setPolarity(double polarity) {
		this.polarity = polarity;
	}

	public double getAnger() {
		return anger;
	}


	public void setAnger(double anger) {
		this.anger = anger;
	}

	public double getAnxiety() {
		return anxiety;
	}


	public void setAnxiety(double anxiety) {
		this.anxiety = anxiety;
	}


	public double getSadness() {
		return sadness;
	}


	public void setSadness(double sadness) {
		this.sadness = sadness;
	}
	
	public double getPositive() {
		return positive;
	}


	public void setPositive(double positive) {
		this.positive = positive;
	}


	public double getHate() {
		return hate;
	}


	public void setHate(double hate) {
		this.hate = hate;
	}


	public double getHappy() {
		return happy;
	}


	public void setHappy(double happy) {
		this.happy = happy;
	}


	public double getSurprise() {
		return surprise;
	}


	public void setSurprise(double surprise) {
		this.surprise = surprise;
	}
	
}
