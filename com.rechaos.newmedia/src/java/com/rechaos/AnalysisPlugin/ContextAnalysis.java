package com.rechaos.AnalysisPlugin;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.rechaos.lingpipe.PolarityBasic;

public class ContextAnalysis {
	
	public double getAngerRank() {
		return factor.getAnger();
	}

	public double getAnxietyRank() {
		return factor.getAnxiety();
	}

	public double getHappyRank() {
		return factor.getHappy();
	}
	
	public double getSadnessRank() {
		return factor.getSadness();
	}

	public double getPositiveEmtionRank() {
		return factor.getPositive();
	}

	public double getPositiveRank() {
		return Math.abs(factor.getPolarity());
	}
	
	public double getHateRank(){
		return factor.getHate();
	}

	ContextAnalysis(){};
	
	ContextAnalysis(String context, String rootPath){
		this.context = context;
		this.rootPath = rootPath;
		PolarityDic dic = new PolarityDic();
		dic.setDic(DicUtil.LoadDicByExcel(""));
		TextRankKeyword rankword = new TextRankKeyword(30);
		List<Entry<String, Float>> result = rankword.getKeyword(rootPath, this.context);
		System.out.println("====================================");
		System.out.println("分析结果：");
		factor = new FactorCounter();
		for(Entry<String,Float> e : result){
			EmtionEntry emtion = dic.getWordPolarity(e.getKey());
			if(emtion == null){
				continue;
			}
			factor.PolarityValue(emtion.getPolartiy(), emtion.getStrength(), e.getValue());
			factor.AngerValue(emtion.getType(), emtion.getStrength(), e.getValue());
			factor.AnxietyValue(emtion.getType(), emtion.getStrength(), e.getValue());
			factor.HappinessValue(emtion.getType(), emtion.getStrength(), e.getValue());
			factor.SadnessValue(emtion.getType(), emtion.getStrength(), e.getValue());
			factor.HateValue(emtion.getType(), emtion.getStrength(), e.getValue());
			factor.PositiveValue(emtion.getType(), emtion.getStrength(), e.getValue());
			System.out.print(e);
			System.out.println("\t"+dic.getWordPolarity(e.getKey()));
		}
		System.out.println("得分结果：");
		System.out.println("polarity="+factor.getPolarity()+"\t");
		System.out.println("anger="+factor.getAnger()+"\t");
		System.out.println("happy="+factor.getHappy()+"\t");
		System.out.println("anxiety="+factor.getAnxiety()+"\t");
		System.out.println("sadness="+factor.getSadness()+"\t");
		System.out.println("Positiveemtion="+(factor.getPositive())+"\t");
		System.out.println("hate="+factor.getHate()+"\t");
		System.out.println("=====================================");
	}
	
	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}
	
	public String getRootPath() {
		return rootPath;
	}

	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	}



	private String context;
	
	private FactorCounter factor;
	
	private String rootPath;


}
