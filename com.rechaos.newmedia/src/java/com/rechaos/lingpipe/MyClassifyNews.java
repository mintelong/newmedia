package com.rechaos.lingpipe;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import com.aliasi.classify.Classification;
import com.aliasi.classify.Classified;
import com.aliasi.classify.ConfusionMatrix;
import com.aliasi.classify.DynamicLMClassifier;
import com.aliasi.classify.JointClassification;
import com.aliasi.classify.JointClassifier;
import com.aliasi.classify.JointClassifierEvaluator;
import com.aliasi.lm.NGramProcessLM;
import com.aliasi.util.AbstractExternalizable;
import com.aliasi.util.Files;
import com.rechaos.util.FileUtil;
import com.rechaos.util.LingpipeConstants;

public class MyClassifyNews {

    private static File TESTING_DIR
        =  new File("sogou/test");
    //定义分类  
    private static String[] CATEGORIES = { "汽车","财经","IT","健康","体育","旅游","教育","招聘","文化","军事" };
    private static String[] TAGS = { "auto","economy","tech","health","sport","travle","edu","job","culture","politics" };

    private static int NGRAM_SIZE = 6;
    
    static JointClassifier<CharSequence> mSubjectivityClassifier;

    public void test()
        throws ClassNotFoundException, IOException {

        DynamicLMClassifier<NGramProcessLM> classifier
            = DynamicLMClassifier.createNGramProcess(CATEGORIES,NGRAM_SIZE);
 
        FileInputStream fileIn = new FileInputStream("tclassifier.model");
        ObjectInputStream objIn = new ObjectInputStream(fileIn);
        @SuppressWarnings("unchecked")
        JointClassifier<CharSequence> subjectivityClassifier
            = (JointClassifier<CharSequence>) objIn.readObject();
        mSubjectivityClassifier = subjectivityClassifier;
        objIn.close();
        
        //compiling
        System.out.println("Compiling");
        @SuppressWarnings("unchecked") // we created object so know it's safe
        JointClassifier<CharSequence> compiledClassifier
            = (JointClassifier<CharSequence>)
            AbstractExternalizable.compile(classifier);

        boolean storeCategories = true;
        JointClassifierEvaluator<CharSequence> evaluator
            = new JointClassifierEvaluator<CharSequence>(compiledClassifier,
                                                         CATEGORIES,
                                                         storeCategories);
        for(int i = 0; i < CATEGORIES.length; ++i) {
            File classDir = new File(TESTING_DIR,CATEGORIES[i]);
            String[] testingFiles = classDir.list();
            for (int j=0; j<testingFiles.length;  ++j) {
                String text
                    = Files
                    .readFromFile(new File(classDir,testingFiles[j]),"utf-8");
                System.out.print("Testing on " + CATEGORIES[i] + "/" + testingFiles[j] + " ");
                Classification classification
                    = new Classification(CATEGORIES[i]);
                Classified<CharSequence> classified
                    = new Classified<CharSequence>(text,classification);
                evaluator.handle(classified);
                JointClassification jc =
                		mSubjectivityClassifier.classify(text);
                
                String bestCategory = jc.bestCategory();
                String details = jc.toString();
                System.out.println("Got best category of: " + bestCategory);
                System.out.println(jc.toString());
                System.out.println("---------------");
            }
        }
        ConfusionMatrix confMatrix = evaluator.confusionMatrix();
        System.out.println("Total Accuracy: " + confMatrix.totalAccuracy());

        System.out.println("\nFULL EVAL");
        System.out.println(evaluator);
    }
    
    private void init() throws IOException,ClassNotFoundException{
    	if(mSubjectivityClassifier == null){
//    		 FileInputStream fileIn = new FileInputStream("web-app/lingpipe/model/tclassifier.model");
//    		 String root = FileUtil.getProjectRootPath();
//    	     FileInputStream fileIn = new FileInputStream(root+"/../webapps/51webdata/lingpipe/model/tclassifier.model");
    		FileInputStream fileIn = null;
    		ObjectInputStream objIn;
    		try{
    			fileIn = new FileInputStream(LingpipeConstants.LINGPIPE_APP_CATEGORY);
    			objIn = new ObjectInputStream(fileIn);
    		}catch(Exception e){
    			fileIn = new FileInputStream(LingpipeConstants.LINGPIPE_DEV_CATEGORY);
    			objIn = new ObjectInputStream(fileIn);
    		}
//    	     ObjectInputStream objIn = new ObjectInputStream(fileIn);
    	     @SuppressWarnings("unchecked")
    	     JointClassifier<CharSequence> subjectivityClassifier
    	            = (JointClassifier<CharSequence>) objIn.readObject();
    	     mSubjectivityClassifier = subjectivityClassifier;
    	     objIn.close();
    	}
    }
    
    public String category(String content) throws ClassNotFoundException, IOException{
    	init();
    	String text = new String(content.getBytes("gbk"));
    	JointClassification jc = mSubjectivityClassifier.classify(text);
//    	System.out.println("Got best category of: " + jc.bestCategory());
//    	System.out.println(jc.toString());
    	String tmp = jc.bestCategory();
    	String result = "";
    	for(int i=0;i<CATEGORIES.length;i++){
    		if(tmp.equals(CATEGORIES[i])){
    			result = TAGS[i];
    			break;
    		}
    	}
    	return result;
    }
    
    public static void main(String[] args) throws ClassNotFoundException, IOException{
    	new MyClassifyNews().category("早睡早起");
    }
}
