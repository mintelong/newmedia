package com.rechaos.lingpipe;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.aliasi.classify.Classification;
import com.aliasi.classify.Classified;
import com.aliasi.classify.DynamicLMClassifier;
import com.aliasi.classify.LMClassifier;
import com.aliasi.lm.LanguageModel;
import com.aliasi.lm.NGramProcessLM;
import com.aliasi.stats.MultivariateDistribution;
import com.aliasi.util.Files;

public class CopyOfPolarityBasic {
	String content;
    File mPolarityDir;
    String[] mCategories;
    //语言模型分类器,用来对训练的数据进行分类
    DynamicLMClassifier<NGramProcessLM> mClassifier;
    
    public CopyOfPolarityBasic(String content) {
		super();
		this.content = content;
	}

	LMClassifier<LanguageModel,MultivariateDistribution> lmClassifier;
    
    CopyOfPolarityBasic() {
        System.out.println("\nBASIC POLARITY DEMO");
        
      //指定语料库
        mPolarityDir = new File("models\\comment");
    //    mPolarityDir = new File("/usr/lib/tools/tomcat/apache-tomcat-7.0.52/webapps/newmedia/models/comment");
        
        System.out.println("\nData Directory=" + mPolarityDir);
        
        //通过文件夹获得类型
        mCategories = mPolarityDir.list();
        //N-Gram是大词汇连续语音识别中常用的一种语言模型
        //这些概率可以通过直接从语料中统计N个词同时出现的次数得到。
        int nGram = 8;
        
        mClassifier 
            = DynamicLMClassifier
            .createNGramProcess(mCategories,nGram);
    }

    @SuppressWarnings("unchecked")
	String run() throws ClassNotFoundException, IOException {
       // train();
        
        FileInputStream fileIn = new FileInputStream("models\\subjectivity.model");
    //	FileInputStream fileIn = new FileInputStream("/usr/lib/tools/tomcat/apache-tomcat-7.0.52/webapps/newmedia/models/subjectivity.model");
    	
        ObjectInputStream objIn = new ObjectInputStream(fileIn);
        lmClassifier = (LMClassifier<LanguageModel, MultivariateDistribution>) objIn.readObject();
        objIn.close();
        evaluate();
        Classification classification  = lmClassifier.classify(content);

		return classification.bestCategory();
    }

    boolean isTrainingFile(File file) {
        return file.getName().charAt(2) != '9';  // test on fold 9
    }

    /***
     * 对数据进行训练
     * @throws IOException
     */
    void train() throws IOException {
        int numTrainingCases = 0;
        int numTrainingChars = 0;
        System.out.println("\nTraining.");
        //遍历所有类别
        for (int i = 0; i < mCategories.length; ++i) {
        	
        	//从文件名获得类型，文件名为:pos、neg、
            String category = mCategories[i];
            //A Classification provides a first-best category. Subclasses provide n-best results with numerical scores of various interpretations
            Classification classification
                = new Classification(category);
            File file = new File(mPolarityDir,mCategories[i]);
            File[] trainFiles = file.listFiles();
            
            //遍历所有文件
            for (int j = 0; j < trainFiles.length; ++j) {
                File trainFile = trainFiles[j];
                if(trainFile.isDirectory()) continue;
               // if (isTrainingFile(trainFile)) {
                    ++numTrainingCases;
                    String review = Files.readFromFile(trainFile,"utf-8");
                    numTrainingChars += review.length();
                    //创建一个分类对象，review为文本内容，classification为文本类型
                    Classified<CharSequence> classified
                        = new Classified<CharSequence>(review,classification);
                    //训练分类器
                    mClassifier.handle(classified);
                //}
            }
        }
        System.out.println("  # Training Cases=" + numTrainingCases);
        System.out.println("  # Training Chars=" + numTrainingChars);
        
        System.out.println("\nCompiling.\n  Model file=subjectivity.model");
        FileOutputStream fileOut = new FileOutputStream("subjectivity.model");
        ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
        mClassifier.compileTo(objOut);
        objOut.close();
    }
    
    

    void evaluate() throws IOException {
   /*     System.out.println("\nEvaluating.");
        int numTests = 0;
        int numCorrect = 0;
      //遍历所有类型
        for (int i = 0; i < mCategories.length; ++i) {
            String category = mCategories[i];
            File file = new File("POLARITY_DIR/txt_sentoken",category);
            File[] trainFiles = file.listFiles();
            //遍历所有类型下的文件文件
            for (int j = 0; j < trainFiles.length; ++j) {
            	
                File trainFile = trainFiles[j];
                if (trainFile.isFile()) {
                    String review = Files.readFromFile(trainFile,"gbk");
                    ++numTests;
                  //使用分类器进行评估，和概率计算，用得出最高分进行极性分析
                    Classification classification
                        = lmClassifier.classify(review);
                  //如果分类正确就自增
                    if (classification.bestCategory().equals(category))
                        ++numCorrect;
                    System.out.println(mPolarityDir + "\\" + mCategories[i] + "\\" + trainFile.getName() + " sentiment is:" + classification.bestCategory());
                }
            }
        }*/
//    	  System.out.println("\nEvaluating.");
//
//              File file = new File("POLARITY_DIR/txt_sentoken");
//              File[] trainFiles = file.listFiles();
//              //遍历所有类型下的文件文件
//              for (int j = 0; j < trainFiles.length; ++j) {
//              	
//                  File trainFile = trainFiles[j];
//                  if (trainFile.isFile()) {
//                      String review = Files.readFromFile(trainFile,"gbk");
//                    //使用分类器进行评估，和概率计算，用得出最高分进行极性分析
//                      Classification classification
//                          = lmClassifier.classify(review);
//
//                      System.out.println(trainFile.getName() + " sentiment is:" + classification.bestCategory());
//                      System.out.println(classification.toString());
//                  }
//              }
          Classification classification  = lmClassifier.classify(content);

          System.out.println(" sentiment is:" + classification.bestCategory());
    }

    public static void main(String[] args) {
    	CopyOfPolarityBasic cp = new CopyOfPolarityBasic();
        /*try {
            new CopyOfPolarityBasic("漂亮").run();
        } catch (Throwable t) {
            System.out.println("Thrown: " + t);
            t.printStackTrace(System.out);
        }*/
    }

}

