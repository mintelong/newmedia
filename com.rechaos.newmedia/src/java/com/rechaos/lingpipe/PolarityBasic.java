package com.rechaos.lingpipe;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

import com.aliasi.classify.Classification;
import com.aliasi.classify.Classified;
import com.aliasi.classify.DynamicLMClassifier;
import com.aliasi.classify.JointClassification;
import com.aliasi.classify.LMClassifier;
import com.aliasi.lm.LanguageModel;
import com.aliasi.lm.NGramProcessLM;
import com.aliasi.stats.MultivariateDistribution;
import com.aliasi.util.Files;
import com.rechaos.util.FileUtil;
import com.rechaos.util.LingpipeConstants;

public class PolarityBasic {

    File mPolarityDir;
    String[] mCategories;
    //语言模型分类器,用来对训练的数据进行分类
    DynamicLMClassifier<NGramProcessLM> mClassifier;
    
    LMClassifier<LanguageModel,MultivariateDistribution> lmClassifier;
    
    public PolarityBasic() {
        System.out.println("\nBASIC POLARITY DEMO");
        
        //指定语料库
        mPolarityDir = new File("D:/SDK/POLARITY_DIR/all");
        
        System.out.println("\nData Directory=" + mPolarityDir);
        
        //通过文件夹获得类型
        mCategories = mPolarityDir.list();
        //N-Gram是大词汇连续语音识别中常用的一种语言模型
        //这些概率可以通过直接从语料中统计N个词同时出现的次数得到。
        int nGram = 7;
        
        mClassifier 
            = DynamicLMClassifier
            .createNGramProcess(mCategories,nGram);
    }

    @SuppressWarnings("unchecked")
	void run() throws ClassNotFoundException, IOException {
//        train();
        
        FileInputStream fileIn = new FileInputStream("D:/SDK/subjectivity.model");
        ObjectInputStream objIn = new ObjectInputStream(fileIn);
        lmClassifier = (LMClassifier<LanguageModel, MultivariateDistribution>) objIn.readObject();
        objIn.close();
        evaluate();
    }

    boolean isTrainingFile(File file) {
        return file.getName().charAt(2) != '6';  // test on fold 9
//    	return false;
    }
  
    /***
     * 对数据进行训练
     * @throws IOException
     */
    void train() throws IOException {
        int numTrainingCases = 0;
        int numTrainingChars = 0;
        System.out.println("\nTraining.");
        //遍历所有类别
        for (int i = 0; i < mCategories.length; ++i) {
        	
        	//从文件名获得类型，文件名为:pos、neg、
            String category = mCategories[i];
            //A Classification provides a first-best category. Subclasses provide n-best results with numerical scores of various interpretations
            Classification classification
                = new Classification(category);
            File file = new File(mPolarityDir,mCategories[i]);
            File[] trainFiles = file.listFiles();
            
            //遍历所有文件
            for (int j = 0; j < trainFiles.length; ++j) {
                File trainFile = trainFiles[j];
                if(trainFile.isDirectory()) continue;
               // if (isTrainingFile(trainFile)) {
                    ++numTrainingCases;
                    String review = Files.readFromFile(trainFile,"gbk");
                    numTrainingChars += review.length();
                    //创建一个分类对象，review为文本内容，classification为文本类型
                    Classified<CharSequence> classified
                        = new Classified<CharSequence>(review,classification);
                    //训练分类器
                    mClassifier.handle(classified);
                //}
            }
        }
        System.out.println("  # Training Cases=" + numTrainingCases);
        System.out.println("  # Training Chars=" + numTrainingChars);
        
        System.out.println("\nCompiling.\n  Model file=subjectivity.model");
        FileOutputStream fileOut = new FileOutputStream("D:/SDK/subjectivity.model");
        ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
        mClassifier.compileTo(objOut);
        objOut.close();
    }
    
    

    void evaluate() throws IOException {
//    	System.out.println("\nEvaluating.");
//        int numTests = 0;
//        int numCorrect = 0;
//        for (int i = 0; i < mCategories.length; ++i) {
//            String category = mCategories[i];
//            File file = new File(mPolarityDir,mCategories[i]);
//            File[] trainFiles = file.listFiles();
//            for (int j = 0; j < trainFiles.length; ++j) {
//                File trainFile = trainFiles[j];
//                if (!isTrainingFile(trainFile)) {
//                    String review = Files.readFromFile(trainFile,"gbk");
//                    ++numTests;
//                    Classification classification
//                        = lmClassifier.classify(review);
//                    if (classification.bestCategory().equals(category))
//                        ++numCorrect;
//                }
//            }
//        }
//        System.out.println("  # Test Cases=" + numTests);
//        System.out.println("  # Correct=" + numCorrect);
//        System.out.println("  % Correct=" 
//                           + ((double)numCorrect)/(double)numTests);
   
    	  System.out.println("\nEvaluating.");

              File file = new File("C:/Users/lenovo/Desktop/articles");
              File[] trainFiles = file.listFiles();
              //遍历所有类型下的文件文件
              for (int j = 0; j < trainFiles.length; ++j) {
              	
                  File trainFile = trainFiles[j];
                  if (trainFile.isFile()) {
                      String review = Files.readFromFile(trainFile,"UTF-8");
                    //使用分类器进行评估，和概率计算，用得出最高分进行极性分析
                      JointClassification classification
                          = lmClassifier.classify(review);

                      System.out.println(trainFile.getName() + " sentiment is:" + classification.bestCategory());
		              	System.out.println("--------------------");
		            	System.out.println(review);
		            	System.out.println(classification.bestCategory());
		//                	System.out.println(temp + ":" +temp);
		            	System.out.println(classification.conditionalProbability(classification.bestCategory()));
                  }
              }
    }

    public Map<String,String>  feel(String content) throws Exception{
    	Map<String,String> result = new HashMap<String,String>();
//    	FileInputStream fileIn = new FileInputStream("web-app/lingpipe/model/subjectivity.model");
//    	String root = FileUtil.getProjectRootPath();
//    	FileInputStream fileIn = new FileInputStream(root+"/../webapps/51webdata/lingpipe/model/subjectivity.model");
    	FileInputStream fileIn = null;
    	ObjectInputStream objIn;
		try{
			fileIn = new FileInputStream("D:/SDK/subjectivity.model");
			objIn = new ObjectInputStream(fileIn);
		}catch(Exception e){
			fileIn = new FileInputStream(LingpipeConstants.LINGPIPE_DEV_FEEL);
			objIn = new ObjectInputStream(fileIn);
		}
    	
//    	
////        ObjectInputStream objIn = new ObjectInputStream(fileIn);
        lmClassifier = (LMClassifier<LanguageModel, MultivariateDistribution>) objIn.readObject();
//        objIn.close();
        JointClassification jClassification = lmClassifier.classify(content);
//    	Classification classification = lmClassifier.classify(content);
//    	System.err.println(jClassification.bestCategory());
    	double temp = jClassification.conditionalProbability(0);
    	
    	String category = jClassification.bestCategory();
    	result.put("category", category);
    	result.put("conditionalPro", String.valueOf(temp));
    	System.out.println("--------------------");
    	System.out.println(content);
    	System.out.println(category);
//    	System.out.println(temp + ":" +temp);
    	System.out.println(jClassification.conditionalProbability(category));
    	return result;
    }
    
    public static void main(String[] args) {
        try {
        	PolarityBasic t =  new PolarityBasic();
        	t.run();
//        	t.feel("这玩意的准确率也太低了吧，只要文本长一点，基本无解了啊");
//        	t.feel("真是逗比，一点都不准");
//        	t.feel("好逗，不解释");
//        	t.feel("觉得没什么用");
//        	t.feel("还行，物有所值");
//        	t.feel("赞！");
//        	t.feel("但是公交指示不对,如果是蔡陆线的话,会非常麻烦.建议用别的路线");
//        	t.feel("check out的时候很郁闷，我都等半天了，总台小姐竟然先办人家插队的人，鄙视，懒得去争了");
        } catch (Throwable t) {
        	t.printStackTrace();
//            System.out.println("Thrown: " + t);
//            t.printStackTrace(System.out);
        }
    }

}

