package com.rechaos.lingpipe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.ansj.domain.Term;
import org.ansj.splitWord.Analysis;
import org.ansj.splitWord.analysis.ToAnalysis;

public class AnsjTerm {
	/**
	 * 中文分词
	 * @param uid
	 * @param context
	 */
	public void StringSplit(String uid,List<String> context) {
		StringBuffer sb;
		Analysis udf = null;
		Term term;
		for (String c : context) {
			sb = new StringBuffer();
			try {
				udf = new ToAnalysis(new BufferedReader(new StringReader(c)));
				while ((term = udf.next()) != null) {
					System.out.println(term.getName());
					sb.append(term.getName() + " ");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
//			System.out.println(sb.toString());
//			FileUtil.writing(sb.toString(),DataCenterConstants.SM_CSP_DATAAPI_TERM_PATH + uid+".txt", "utf-8");
		}
	}
	
	/**
	 * 中文分词
	 * @param str
	 * @return
	 */
	public static String StringSplit(String str) {
		StringBuffer sb = new StringBuffer();
		Analysis udf = null;
		Term term;
		try {
			udf = new ToAnalysis(new BufferedReader(new StringReader(str)));
			while ((term = udf.next()) != null) {
				sb.append(term.getName() + " ");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return sb.toString();
	}
	
	
	public static void main(String[] args) {
		AnsjTerm aj = new AnsjTerm();
		List<String> list = new ArrayList<String>();
		list.add("宁财神吸毒被拘 7个月吸毒史面临起诉据宁财神交代，他从2013年12月底开始吸食冰毒至今，已有近7个月的吸毒史。坐在审讯室里的宁财神一再承认自己的错误，对自己的吸毒行为“表示深切的歉意和懊悔”。经查，宁财神因创作2006年播出的一部“长回体古装情景喜剧”成名，现为编剧、作家，曾为某卫视生活服务类节目主持人。目前，宁财神因吸食毒品被朝阳分局依法行政拘留。据知情人透露，宁财神在被抓时仅有一人在吸毒，身边并无其他吸毒人员。宁财神表示，被抓前自己刚刚吸食过毒品，其每次购买毒品花费1000元，已经购买过3次，每次吸毒是200多元的量，现在3000元的毒品已经吸完。昨天，张元工作室发出声明称：“张元导演工作室已向警方确认，此事与张元没有任何关联。声明表示，张元与宁财神只是在公开活动中有过见面，并无私交。”但对于此声明内容，北京警方未予以证实。");
		aj.StringSplit("13", list);
	}
}
