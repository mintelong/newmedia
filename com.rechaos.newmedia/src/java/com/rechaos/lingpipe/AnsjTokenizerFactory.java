package com.rechaos.lingpipe;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.ansj.domain.Term;
import org.ansj.recognition.NatureRecognition;
import org.ansj.splitWord.analysis.ToAnalysis;
import org.ansj.util.FilterModifWord;

public class AnsjTokenizerFactory {
	// add stopwords list to filter
	public List<String> addStopwords(String dir) {
		List<String> list = new ArrayList<String>();
		if (dir == null) {
			System.out.println("no stopwords dir");
			return null;
		}
		// read stoplist
		System.out.println("stopwords: " + dir);

		InputStream is;
		try {
			is = this.getClass().getResourceAsStream(dir);
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String word = br.readLine();
			while (word != null) {
				// 停用词使用词性_stop
				list.add(word);
				word = br.readLine();
			}
		} catch (FileNotFoundException e) {
			System.out.println("No stopword file found");
		} catch (IOException e) {
			System.out.println("stopword file io exception");
		}
		return list;
	}

	public static void main(String[] args) {
		AnsjTokenizerFactory ansj = new AnsjTokenizerFactory();
		// 加入停用词
		/*FilterModifWord.insertStopWords(ansj
				.addStopwords("/newWord/newWordFilter.dic"));*/
		// 加入过滤词性词性
		// FilterModifWord.insertStopNatures("v") ;

		List<Term> parse = ToAnalysis.parse("@斑小漉 还记得我们的之前也拍过这样的照片 ~这妹子有一张很像你~~ 图：@宏贇同學");
		new NatureRecognition(parse).recognition();
		System.out.println(parse);

		// 修正词性并且过滤停用
		parse = FilterModifWord.modifResult(parse);

		System.out.println(parse);

	}

}
