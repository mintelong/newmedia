package com.rechaos.lingpipe;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.ansj.domain.Term;
import org.ansj.splitWord.Analysis;
import org.ansj.splitWord.analysis.ToAnalysis;

import com.aliasi.classify.ConditionalClassification;
import com.aliasi.hmm.HiddenMarkovModel;
import com.aliasi.hmm.HmmDecoder;
import com.aliasi.tag.ScoredTagging;
import com.aliasi.tag.TagLattice;
import com.aliasi.tag.Tagging;
import com.aliasi.tokenizer.RegExTokenizerFactory;
import com.aliasi.tokenizer.TokenizerFactory;
import com.aliasi.util.Streams;

public class CopyOfRunMedPost {

    static TokenizerFactory TOKENIZER_FACTORY 
        = new RegExTokenizerFactory("(-|'|\\d|\\p{L})+|\\S");

    public static void main(String[] args) 
        throws ClassNotFoundException, IOException {
    	CopyOfRunMedPost.tag("","14张不会说话的图片，却是内心最强的震撼，14张不会说话的图片，却是内心最强的震撼");
    }
    public static List<String> tag(String path,String context){
    	List<String> ts = new ArrayList<String>();    
    	try {
//			FileInputStream fileIn = new FileInputStream(path+"/usr/lib/tools/tomcat/apache-tomcat-7.0.52/webapps/newmedia/models/199801.test.HiddenMarkovModel");
			FileInputStream fileIn = new FileInputStream(path+"./models//199801.test.HiddenMarkovModel");
			ObjectInputStream objIn = new ObjectInputStream(fileIn);
			
			//将模型文件转换成隐马尔可夫模型
			HiddenMarkovModel hmm = (HiddenMarkovModel) objIn.readObject();
			Streams.closeQuietly(objIn);
			fileIn.close();
			
			//隐马尔可夫模型解析器
			HmmDecoder decoder = new HmmDecoder(hmm);
			
			BufferedReader bufReader = new BufferedReader(new StringReader("-"+context));    
			String line;
			List<String> tokenList = new ArrayList<String>();

			while ((line = bufReader.readLine()) != null) {
			    Analysis udf = new ToAnalysis(new StringReader(line));
				Term term = udf.next();
				while((term=udf.next())!=null){
					tokenList.add(term.getName());
				}
			}
				
/*            char[] cs = line.toCharArray();

			    Tokenizer tokenizer = TOKENIZER_FACTORY.tokenizer(cs,0,cs.length);
			    String[] tokens = tokenizer.tokenize();
			    List<String> tokenList = Arrays.asList(tokens);
*/
			    Tagging<String> tagging = decoder.tag(tokenList);
//			    if(tagging.size()>0){
//			    	tag = tagging.tag(0);
//			    	System.out.println(tagging.token(0) + "/" + tagging.tag(0) + " ");
//			    }
			    
			    for (int i = 0; i < tagging.size(); ++i){
			    	ts.add(tagging.token(i)+"_"+CopyOfRunMedPost.cix(tagging.tag(i)));
//		        	System.out.println(tagging.token(i) + "/" + tagging.tag(i) + " ");
		        }
			    //nBest(tokenList,decoder);

			    //confidence(tokenList,decoder);
			Streams.closeQuietly(bufReader);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return ts;
    }
    public  static String cix(String cix){
		String re="其它";
		if(cix.startsWith("a")){
			re = "形容词";
		}
		if(cix.startsWith("n")){
			re = "名词";
		}
		if(cix.startsWith("v")){
			re = "动词";
		}
		return re;
	}
    static void firstBest(List<String> tokenList, HmmDecoder decoder) throws IOException {
        Tagging<String> tagging = decoder.tag(tokenList);
        System.out.println("\nFIRST BEST");
        BufferedWriter bw = new BufferedWriter(new FileWriter(new File("library\\1.out.txt")));
        for (int i = 0; i < tagging.size(); ++i)
        	bw.write(tagging.token(i) + "/" + tagging.tag(i) + " ");
        bw.close();
    }

    static final int MAX_N_BEST = 5;

    static void nBest(List<String> tokenList, HmmDecoder decoder) {
        System.out.println("\nN BEST");
        System.out.println("#   JointLogProb         Analysis");
        Iterator<ScoredTagging<String>> nBestIt = decoder.tagNBest(tokenList,MAX_N_BEST);
        for (int n = 0; n < MAX_N_BEST && nBestIt.hasNext(); ++n) {
            ScoredTagging<String> scoredTagging = nBestIt.next();
            double score = scoredTagging.score();
            System.out.print(n + "   " + format(score) + "  ");
            for (int i = 0; i < tokenList.size(); ++i)
                System.out.print(scoredTagging.token(i) + "_" + pad(scoredTagging.tag(i),5));
            System.out.println();
        }        
    }

    static void confidence(List<String> tokenList, HmmDecoder decoder) {
        System.out.println("\nCONFIDENCE");
	System.out.println("#   Token          (Prob:Tag)*");
        TagLattice<String> lattice = decoder.tagMarginal(tokenList);
        for (int tokenIndex = 0; tokenIndex < tokenList.size(); ++tokenIndex) {
            ConditionalClassification tagScores = lattice.tokenClassification(tokenIndex);
	    System.out.print(pad(Integer.toString(tokenIndex),4));
	    System.out.print(pad(tokenList.get(tokenIndex),15));
            for (int i = 0; i < 5; ++i) {
		double conditionalProb = tagScores.score(i);
		String tag = tagScores.category(i);
                System.out.print(" " + format(conditionalProb) 
				 + ":" + pad(tag,4));
	    }
	    System.out.println();
	}
    }

    static String format(double x) {
	return String.format("%9.3f",x);
    }

    static String pad(String in, int length) {
	if (in.length() > length) return in.substring(0,length-3) + "...";
	if (in.length() == length) return in;
	StringBuilder sb = new StringBuilder(length);
	sb.append(in);
	while (sb.length() < length) sb.append(' ');
	return sb.toString();
	
    }
}
