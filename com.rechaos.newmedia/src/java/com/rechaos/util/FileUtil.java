package com.rechaos.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;

public class FileUtil {
	//private static final Log LOG = LogFactory.getLog(FileUtil.class);
	/**
	 * reading file
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public static List<String> reading(String fileName) throws IOException{
		  FileInputStream f = new FileInputStream(fileName);
		  BufferedReader dr=new BufferedReader(new InputStreamReader(f)); 
		  List<String> list = new ArrayList<String>();
		  String tmp = "";
		  while((tmp = dr.readLine()) != null){
			  list.add(tmp);
		  }
		  dr.close();
		  return list;
	}
	public static List<String> reading(String fileName,String code) throws IOException{
		  FileInputStream f = new FileInputStream(fileName);
		  BufferedReader dr=new BufferedReader(new InputStreamReader(f, code)); 
		  List<String> list = new ArrayList<String>();
		  String tmp = "";
		  while((tmp = dr.readLine()) != null){
			  list.add(tmp);
		  }
		  dr.close();
		  return list;
	}
	
	/**
	 * reading file
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public static String readingFile(String fileName) throws IOException{
		  FileInputStream f = new FileInputStream(fileName);
		  BufferedReader dr=new BufferedReader(new InputStreamReader(f)); 
		  String readStr = "";
		  String tmp = "";
		  while((tmp = dr.readLine()) != null){
			  readStr +=tmp;
		  }
		  dr.close();
		  return readStr;
	}
	
	public static String readingFile(String fileName, String code) throws IOException{
		  FileInputStream f = new FileInputStream(fileName);
		  BufferedReader dr=new BufferedReader(new InputStreamReader(f, code)); 
		  String readStr = "";
		  String tmp = "";
		  while((tmp = dr.readLine()) != null){
			  readStr +=tmp;
		  }
		  dr.close();
		  return readStr;
	}
	
	/**
	 * get current project path
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public static String getProjectRootPath(){
		//String rootPath=getClass().getResource("/").getFile().toString();		
		String rootPath=System.getProperty("user.dir");

		return rootPath;
	}

	/**
	 * writing file from list collection
	 * 
	 * @param terms
	 * @param outputFile
	 * @throws IOException
	 */
	public static void writing(String terms, String outputFile, String encoder) {
		try {
			FileOutputStream o = new FileOutputStream(outputFile,true);
			BufferedWriter dw = new BufferedWriter(new OutputStreamWriter(o,encoder));
			dw.write(terms);
			dw.newLine();
			dw.close();
		} catch (FileNotFoundException e) {
		//	LOG.error("file not found exception" + e.getMessage());
		} catch (IOException e) {
		//	LOG.error("writing file exception" + e.getMessage());
		}
	}
	
	/**
	 * wirting file from list collection
	 * @param terms
	 * @param outputFile
	 * @throws IOException
	 */
	public static void writing(List<String> terms, String outputFile,String encoder) {
		  try {
			FileOutputStream o = new FileOutputStream(outputFile);
			  BufferedWriter dw = new BufferedWriter(new OutputStreamWriter(o,encoder));
			  for (String s : terms) {
				dw.write(s);
				dw.newLine();
			  }
			  dw.close();
		} catch (FileNotFoundException e) {
		//	LOG.error("file not found exception" + e.getMessage());
		} catch (IOException e) {
		//	LOG.error("writing file exception" + e.getMessage());
		}		
	}
	
	/**
	 * wirting file from list collection
	 * @param terms
	 * @param outputFile
	 * @throws IOException
	 */
	public static void writing(List<String> terms, String outputFile,String encoder,boolean isAppend) {
		  try {
			FileOutputStream o = new FileOutputStream(outputFile,isAppend);
			  BufferedWriter dw = new BufferedWriter(new OutputStreamWriter(o,encoder));
			  for (String s : terms) {
				dw.write(s);
				dw.newLine();
			}
			  dw.close();
		} catch (FileNotFoundException e) {
		//	LOG.error("file not found exception" + e.getMessage());
		} catch (IOException e) {
		//	LOG.error("writing file exception" + e.getMessage());
		}
	}
}
