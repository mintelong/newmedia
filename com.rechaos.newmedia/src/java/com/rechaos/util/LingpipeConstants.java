package com.rechaos.util;

public class LingpipeConstants {
	public static final String ROOT = FileUtil.getProjectRootPath();
	public static final String LINGPIPE_APP_ENTITY = ROOT+ "/../../lingpipe/model/entities.model";
	public static final String LINGPIPE_DEV_ENTITY = ROOT+ "/web-app/lingpipe/model/entities.model";
	public static final String LINGPIPE_APP_FEEL = ROOT+ "/../../lingpipe/model/subjectivity.model";
	public static final String LINGPIPE_DEV_FEEL = ROOT+ "/web-app/lingpipe/model/subjectivity.model";
	public static final String LINGPIPE_APP_CATEGORY = ROOT+ "/../../lingpipe/model/tclassifier.model";
	public static final String LINGPIPE_DEV_CATEGORY = ROOT+ "/web-app/lingpipe/model/tclassifier.model";
}