$(function(){
	if ($('.sider').length > 0) {
		if ($('.header .btn-sider').length > 0) {
			$('.header .btn-sider').tap(function(){
				switchSider(true);
			});
		}

		$('body').on('tap', '.sider-cover', function(){
			switchSider(false);
		});
		$('body').on('swipeRight', '.sider-cover, .sider', function(){
			switchSider(false);
		});
	}

})

function switchSider(isOpen) {
	if (isOpen) {
		if ($('.sider-cover').length <= 0) {
			$('body').append('<div class="sider-cover"></div>');
		}
		$('.sider-cover').show();
		$('.sider').addClass('open');
	} else {
		$('.sider').removeClass('open');
		$('.sider-cover').hide().remove();
	}
}