<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>热巢数据分析系统</title>
		
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'index.css')}" type="text/css">
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery-ui.css')}" type="text/css">
        
        <script src="${resource(dir: 'js', file: 'jquery-ui.js')}" type="text/javascript"></script>
		
	</head>
	<body>
		
		
		
		
	<div class="page-header">
	<h1>热巢数据分析系统</h1>
	</div>
	
	<ul class="nav nav-tabs" role="tablist" id="myTab">
		<li role="presentation" class="active"><a href="/newmedia/login/analysis" aria-controls="home" role="tab" data-toggle="tab">文章分析</a></li>
		<li role="presentation"><a href="/newmedia/login/show" aria-controls="profile" role="tab" data-toggle="tab">数据显示</a></li>
	</ul>
	
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="home">
			<div class="panel panel-default">
				<div class="panel-heading"></div>
				<div class="panel-body">
				
					<form action="/newmedia/login/addArticle" method="post" role="form" id="articleform">
						<div class="row">
							<div class="col-md-4">
								<input type="text" name="title" class="form-control" id="title" placeholder="请输入标题..." required>
								<textarea class="form-control" name="content" rows="20" id="content" placeholder="请输入文章内容..." required></textarea>
							</div>
							<div class="col-md-8">
								<div class="row">
									<div class="col-md-4">
										<input type="number" name="wechats" class="form-control" id="wechats" required> <label for="wechat">微信分享次数</label>
									</div>
									<div class="col-md-4">
										<input type="number" name="pageviews" class="form-control" id="pageviews" required> <label for="pageviews">点击次数</label>
									</div>
									<div class="col-md-4">
										<input type="number" name="picnum" class="form-control" id="picnum" required> <label for="picnum">图片数量</label>
									</div>
									<div class="col-md-4">
										<input type="text" id="datepicker1" name="pubdate" class="form-control" required> <label for="pubdate">发布日期</label>
									</div>
								</div>
								
								
								<div class="row">
									<div class="col-md-1">
										<input type="checkbox" name="type1" class="form-control" id="type1" > <label for="type1">明星</label>
									</div>
									<div class="col-md-1">
										<input type="checkbox" name="type2" class="form-control" id="type2" > <label for="type2">爱情</label>
									</div>
									<div class="col-md-1">
										<input type="checkbox" name="type3" class="form-control" id="type3" > <label for="type3">电视</label>
									</div>
									<div class="col-md-1">
										<input type="checkbox" name="type4" class="form-control" id="type4" > <label for="type4">怀旧</label>
									</div>
									<div class="col-md-1">
										<input type="checkbox" name="type5" class="form-control" id="type5" > <label for="type5">音乐</label>
									</div>
									<div class="col-md-1">
										<input type="checkbox" name="type6" class="form-control" id="type6" > <label for="type6">科技</label>
									</div>
									<div class="col-md-1">
										<input type="checkbox" name="type7" class="form-control" id="type7" > <label for="type7">时事</label>
									</div>
									<div class="col-md-1">
										<input type="checkbox" name="type8" class="form-control" id="type8" > <label for="type8">运动</label>
									</div>
									<div class="col-md-1">
										<input type="checkbox" name="type9" class="form-control" id="type9" > <label for="type9">动物</label>
									</div>
									
									<div class="col-md-1">
										<input type="checkbox" name="type10" class="form-control" id="type10" > <label for="type10">DIY</label>
									</div>
									<div class="col-md-1">
										<input type="checkbox" name="type11" class="form-control" id="type11" > <label for="type11">旅游</label>
									</div>
									<div class="col-md-1">
										<input type="checkbox" name="type12" class="form-control" id="type12" > <label for="type12">健康</label>
									</div>
									<div class="col-md-1">
										<input type="checkbox" name="type13" class="form-control" id="type13" > <label for="type13">金钱</label>
									</div>
									<div class="col-md-1">
										<input type="checkbox" name="type14" class="form-control" id="type14" > <label for="type14">电影</label>
									</div>
									<div class="col-md-1">
										<input type="checkbox" name="type15" class="form-control" id="type15" > <label for="type15">知识</label>
									</div>
									<div class="col-md-1">
										<input type="checkbox" name="type16" class="form-control" id="type16" > <label for="type16">趣事</label>
									</div>
									<div class="col-md-1">
										<input type="checkbox" name="type17" class="form-control" id="type17" > <label for="type17">启迪</label>
									</div>
									<div class="col-md-1">
										<input type="checkbox" name="type18" class="form-control" id="type18" > <label for="type18">视觉</label>
									</div>
									<div class="col-md-1">
										<input type="checkbox" name="type19" class="form-control" id="type19" > <label for="type19">时尚</label>
									</div>
									<div class="col-md-1">
										<input type="checkbox" name="type20" class="form-control" id="type20" > <label for="type20">美食</label>
									</div>
									<div class="col-md-1">
										<input type="checkbox" name="type21" class="form-control" id="type21" > <label for="type21">有料</label>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-8">
								<button type="submit" class="btn btn-default">添加</button>
							</div>
						</div>
					</form>
					
					
					<form class="form-inline" action="/newmedia/login/search">
						<label for="title">标题查询</label> <input name="title" type="text" class="form-control" id="title" placeholder="请输入标题...">
						<label for="pubdate">发布时间查询</label> <input type="text" name="pubdate" class="form-control" id="datepicker2" placeholder="请输入发布时间...">
						<button type="submit" class="btn btn-default">查询</button>
					</form>
					<table class="table">
						<thead>
							<tr>
								<th>#ID</th>
								<th>标题</th>
								<th>微信分享次数</th>
								<th>点击次数</th>
								<th>发布日期</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>						
						<g:each in="${articles}" var="article" status="i">
							<tr>
								<td>${i+1}</td>
								<td>${article.title}</td>
								<td>${article.wechats}</td>
								<td>${article.pageviews}</td>
								<td><g:formatDate format="yyyy-MM-dd" date="${article.pubdate}"></g:formatDate></td>
								<td><g:link action="delete" params='[articleid:"${article.id}"]'>删除</g:link></td>
							</tr>
						</g:each>							
						</tbody>
						
						
	            		
					</table>
					
					
					<div class="pagea">
		                	<span>共${totalCount ?: 0}条记录</span>
			                <div class="page float_r">
								<g:paginate controller="login" action="analysis" total="${totalCount ?: 0}" params=']'/>
							</div>
	            		</div>
	            		
	            		
				</div>
			</div>
		</div>
		
		
		
		
	<script>
	  $(function() {
	    $("#datepicker1").datepicker();
	    $("#datepicker2").datepicker();
	  });
	  </script>
		
		
		
		
		
		
		
	</body>
</html>
