<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>热巢数据分析系统</title>
		
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'index.css')}" type="text/css">
		<script type="text/javascript" src="${resource(dir:'js',file:'util.js') }"></script>
		
		<script type="text/javascript" src="${resource(dir:'js',file:'Chart.min.js') }"></script>
		<script type="text/javascript">
		$(function() {


			$('.sel').change(function(){
            	var name = $(this).attr('id');
            	var val = $(this).find('option:selected').attr('value')
            	if(val!="no"){
            		location.href="/newmedia/login/show?name="+name+"&val="+val+""
	            }
            });

			$('.selcix').change(function(){
            	var namecix = $(this).attr('id');
            	var valcix = $(this).find('option:selected').attr('value')
            	if(valcix!="no"){
            		location.href="/newmedia/login/show?namecix="+namecix+"&valcix="+valcix+""
	            }
            });
            
            
		    Chart.defaults.global.scaleFontFamily = '"Microsoft YaHei New", "Microsoft Yahei", "微软雅黑", 宋体, SimSun, STXihei, "华文细黑", sans-serif';
		    Chart.defaults.global.scaleFontSize = 11;
		    Chart.defaults.global.scaleFontColor = '#000';
		    Chart.defaults.global.tooltipFontFamily = '"Microsoft YaHei New", "Microsoft Yahei", "微软雅黑", 宋体, SimSun, STXihei, "华文细黑", sans-serif';
		    Chart.defaults.global.tooltipFontColor = '#fff';
		    Chart.defaults.global.tooltipFontSize = 11;
		    
		    
		    
		    var data = {
		            labels: [<g:each in="${numCount}" var="num">
		            '${num.key}',
		            </g:each>],
		            datasets: [{
		                label: "My Second dataset",
		                fillColor: "rgba(151,187,205,0.5)",
		                strokeColor: "rgba(151,187,205,0.8)",
		                highlightFill: "rgba(151,187,205,0.75)",
		                highlightStroke: "rgba(151,187,205,1)",
		                data: [<g:each in="${numCount}" var="num">
			            '${num.value}',
			            </g:each>]
		            }]
		        };
		
		        new Chart(document.getElementById("myChart").getContext("2d")).Bar(data);
		
		        var data = {
		            labels: [<g:each in="${numTView}" var="num">
		            '${num.key}',
		            </g:each>],
		            datasets: [{
		                label: "My Second dataset",
		                fillColor: "rgba(151,187,205,0.5)",
		                strokeColor: "rgba(151,187,205,0.8)",
		                highlightFill: "rgba(151,187,205,0.75)",
		                highlightStroke: "rgba(151,187,205,1)",
		                data: [<g:each in="${numTView}" var="num">
			            '${num.value}',
			            </g:each>]
		            }]
		        };
		
		        new Chart(document.getElementById("myChart2").getContext("2d")).Bar(data);
		
		        var data = {
		            labels: [<g:each in="${numTShare}" var="num">
		            '${num.key}',
		            </g:each>],
		            datasets: [{
		                label: "My Second dataset",
		                fillColor: "rgba(151,187,205,0.5)",
		                strokeColor: "rgba(151,187,205,0.8)",
		                highlightFill: "rgba(151,187,205,0.75)",
		                highlightStroke: "rgba(151,187,205,1)",
		                data: [<g:each in="${numTShare}" var="num">
			            '${num.value}',
			            </g:each>]
		            }]
		        };
		
		        new Chart(document.getElementById("myChart3").getContext("2d")).Bar(data);
		
		        var data = {
		            labels: [<g:each in="${numCView}" var="num">
		            '${num.key-step} - ${num.key}',
		            </g:each>],
		            datasets: [{
		                label: "My Second dataset",
		                fillColor: "rgba(151,187,205,0.5)",
		                strokeColor: "rgba(151,187,205,0.8)",
		                highlightFill: "rgba(151,187,205,0.75)",
		                highlightStroke: "rgba(151,187,205,1)",
		                data: [<g:each in="${numCView}" var="num">
			            '${num.value}',
			            </g:each>]
		            }]
		        };
		
		        new Chart(document.getElementById("myChart4").getContext("2d")).Bar(data);
		
		        var data = {
		            labels: [<g:each in="${numCShare}" var="num">
		            '${num.key-step} - ${num.key}',
		            </g:each>],
		            datasets: [{
		                label: "My Second dataset",
		                fillColor: "rgba(151,187,205,0.5)",
		                strokeColor: " (151,187,205,0.8)",
		                highlightFill: "rgba(151,187,205,0.75)",
		                highlightStroke: "rgba(151,187,205,1)",
		                data: [<g:each in="${numCShare}" var="num">
			            '${num.value}',
			            </g:each>]
		            }]
		        };
		
		        new Chart(document.getElementById("myChart5").getContext("2d")).Bar(data);

		        var data = {
			            labels: [<g:each in="${numVShare}" var="num">
			            '${num.key-step} - ${num.key}',
			            </g:each>],
			            datasets: [{
			                label: "My Second dataset",
			                fillColor: "rgba(151,187,205,0.5)",
			                strokeColor: " (151,187,205,0.8)",
			                highlightFill: "rgba(151,187,205,0.75)",
			                highlightStroke: "rgba(151,187,205,1)",
			                data: [<g:each in="${numVShare}" var="num">
				            '${num.value}',
				            </g:each>]
			            }]
			        };
			
			        new Chart(document.getElementById("myChart6").getContext("2d")).Bar(data);
		        
		        
		        
		
		    $('a[href="#profile"]').on('shown.bs.tab', function(e) {
		        
		    });
		});
		</script>	
	</head>
	<body>
	
	
		
		
		
	<div class="page-header">
	<h1>热巢数据分析系统</h1>
	</div>
	<ul class="nav nav-tabs" role="tablist" id="myTab">
		<li role="presentation"><a href="/newmedia/login/analysis" aria-controls="home" role="tab" data-toggle="tab">文章分析</a></li>
		<li role="presentation" class="active"><a href="/newmedia/login/show" aria-controls="profile" role="tab" data-toggle="tab">数据显示</a></li>
	</ul>
	
		
		
		<div role="tabpanel" class="tab-pane" id="profile">
			<div class="panel panel-default">
				<div class="panel-heading">数据概括分析
				<a href="/newmedia/login/runAnalysis" style="color:red">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;执行分析</a>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							<h3>
								<span class="label label-default">文章（标题）中使用最多的词排行：</span>
							</h3>
							
							<table class="table">
								<thead>
									<tr>
										<th>#</th>
										<th>词</th>
										<th>数量</th>
										<th>
										<select  id="title" class="sel" style="width:119px;font-size:12px;font-family:'Microsoft Yahei'">
											<option value="no">感情分类</option>
											<g:if test="${title1.equals("pos") }">
												<option value="pos" selected="selected">正面</option>
											</g:if><g:else>
												<option value="pos">正面</option>
											</g:else>
											<g:if test="${title1.equals("neg") }">
												<option value="neg" selected="selected">负面</option>
											</g:if><g:else>
												<option value="neg">负面</option>
											</g:else>
											<g:if test="${title1.equals("neu") }">
												<option value="neu" selected="selected">中性</option>
											</g:if><g:else>
												<option value="neu">中性</option>
											</g:else>
										</select>
										</th>
										<th><select  id="titlecix" class="selcix" style="width:119px;font-size:12px;font-family:'Microsoft Yahei'">
											<option value="no">词性</option>
											
											<g:if test="${titlecix.equals("形容词") }">
												<option value="形容词" selected="selected">形容词</option>
											</g:if><g:else>
												<option value="形容词">形容词</option>
											</g:else>
											<g:if test="${titlecix.equals("名词") }">
												<option value="名词" selected="selected">名词</option>
											</g:if><g:else>
												<option value="名词">名词</option>
											</g:else>
											<g:if test="${titlecix.equals("动词") }">
												<option value="动词" selected="selected">动词</option>
											</g:if><g:else>
												<option value="动词">动词</option>
											</g:else>
										</select></th>
									</tr>
								</thead>
								<tbody>
									<g:each in="${titles }" var="title" status="i">
										<tr>
											<td>${i+1 }</td>
											<td>${title.word }</td>
											<td>${title.count }</td>
											<td>
												<g:if test="${title.polarity.equals('pos') }">
													正面
												</g:if><g:elseif test="${title.polarity.equals('neg') }">
													负面
												</g:elseif><g:else>
													中性
												</g:else>
											</td>
											<td>${title.cix }</td>
										</tr>
									</g:each>
								</tbody>
							</table>
							
							<div class="pagea">
			                	<span>共${ titleCount?: 0}条记录</span>
				                <div class="page float_r">
									<g:paginate controller="login" action="show" total="${titleCount ?: 0}" params=']'/>
								</div>
							</div>
							
							<h3>
								<span class="label label-default">文章（内容）中使用最多的词排行：</span>
							</h3>
							
							<table class="table">
								<thead>
									<tr>
										<th>#</th>
										<th>词</th>
										<th>数量</th>
										<th><select  id="content" class="sel" style="width:119px;font-size:12px;font-family:'Microsoft Yahei'">
											<option value="no">感情分类</option>
											<g:if test="${content1.equals("pos") }">
												<option value="pos" selected="selected">正面</option>
											</g:if><g:else>
												<option value="pos">正面</option>
											</g:else>
											<g:if test="${content1.equals("neg") }">
												<option value="neg" selected="selected">负面</option>
											</g:if><g:else>
												<option value="neg">负面</option>
											</g:else>
											<g:if test="${content1.equals("neu") }">
												<option value="neu" selected="selected">中性</option>
											</g:if><g:else>
												<option value="neu">中性</option>
											</g:else>
										</select></th>
										<th><select  id="contentcix" class="selcix" style="width:119px;font-size:12px;font-family:'Microsoft Yahei'">
											<option value="no">词性</option>
											<g:if test="${contentcix.equals("形容词") }">
												<option value="形容词" selected="selected">形容词</option>
											</g:if><g:else>
												<option value="形容词">形容词</option>
											</g:else>
											<g:if test="${contentcix.equals("名词") }">
												<option value="名词" selected="selected">名词</option>
											</g:if><g:else>
												<option value="名词">名词</option>
											</g:else>
											<g:if test="${contentcix.equals("动词") }">
												<option value="动词" selected="selected">动词</option>
											</g:if><g:else>
												<option value="动词">动词</option>
											</g:else>
										</select></th>
									</tr>
								</thead>
								<tbody>
									<g:each in="${contents }" var="content" status="i">
										<tr>
											<td>${i+1 }</td>
											<td>${content.word }</td>
											<td>${content.count }</td>
											<td>
												<g:if test="${content.polarity.equals('pos') }">
													正面
												</g:if><g:elseif test="${content.polarity.equals('neg') }">
													负面
												</g:elseif><g:else>
													中性
												</g:else>
											</td>
											<td>${content.cix }</td>
										</tr>
									</g:each>
								</tbody>
							</table>
							
							<div class="pagea">
			                	<span>共${ contentCount?: 0}条记录</span>
				                <div class="page float_r">
									<g:paginate controller="login" action="show" total="${contentCount ?: 0}" params=']'/>
								</div>
							</div>
							
							<h3>
								<span class="label label-default">文章中最流行的tag（标签）：</span>
							</h3>
							<table class="table">
								<thead>
									<tr>
										<th>#</th>
										<th>Tag</th>
										<th>文章数量</th>
										<th>分享次数</th>
									</tr>
								</thead>
								<tbody>
								<g:each in="${types }" var="type" status="i">
									<tr>
										<td>${i+1 }</td>
										<td>${type.split("_")[0] }</td>
										<td>${type.split("_")[1] }</td>
										<td>${type.split("_")[2] }</td>
									</tr>
								</g:each>
								</tbody>
							</table>
							<div class="pagea">
			                	<span>共${ tagCount?: 0}条记录</span>
				                <div class="page float_r">
									<g:paginate controller="login" action="show" total="${tagCount ?: 0}" params=']'/>
								</div>
							</div>
							
							
							<h3>
								<span class="label label-default">文章中最流行情感分类：</span>
							</h3>
							<table class="table">
								<thead>
									<tr>
										<th>#</th>
										<th>情感分类</th>
										<th>文章数量</th>
										<th>分享次数</th>
									</tr>
								</thead>
								<g:each in="${emtions }" var="emtion" status="i">
									<tr>
										<td>${i+1 }</td>
										<td>${emtion.emtionName }</td>
										<td>${emtion.artcleCount }</td>
										<td>${emtion.shareCount }</td>
									</tr>
								</g:each>
							</table>
	            		</div>
						<div class="col-md-6">
							<h3>
								<span class="label label-default">标题中的数字和文章总数分析表：</span>
							</h3>
							<canvas id="myChart" width="400" height="400"></canvas>
							<h3>
								<span class="label label-default">标题中的数字和阅读量分析表：</span>
							</h3>
							<canvas id="myChart2" width="400" height="400"></canvas>
							<h3>
								<span class="label label-default">标题中的数字和微信分享量分析表：</span>
							</h3>
							<canvas id="myChart3" width="400" height="400"></canvas>
							
							
							
							<h3>
								<span class="label label-default">文章长度和微信阅读量分析表：</span>
							</h3>
							<canvas id="myChart4" width="400" height="400"></canvas>							
							<h3>
								<span class="label label-default">文章长度和微信分享量分析表：</span>
							</h3>
							<canvas id="myChart5" width="400" height="400"></canvas>
							
							<h3>
								<span class="label label-default">阅读量和分析量分析表：</span>
							</h3>
							<canvas id="myChart6" width="400" height="400"></canvas>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		
		
		
		
		
		
		
		
	</body>
</html>
