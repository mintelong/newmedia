<html>
<head>
	<title>上传</title>
	
	
	
	<link href="/newmedia/css/uploadify.css" rel="stylesheet" type="text/css" />
    <script src="/newmedia/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="/newmedia//js/jquery.uploadify.min.js" type="text/javascript"></script>
    
</head>
<body>

<!--  <form action="upload/upload" method="post" enctype="multipart/form-data">-->
	选择文件：<input type="file" name="file" id="uploadify"><br/>
	<a href="javascript:$('#uploadify').uploadify('upload')">上传</a>| 
    <a href="javascript:$('#uploadify').uploadify('cancel')">取消上传</a>
<!--	<input type="submit" value="提交">
</form>-->
<div id='pregress'></div>
<div id='context'>

</div>

<script type="text/javascript">
        $(function () {
            $("#uploadify").uploadify({
                //指定swf文件
                'swf': '/newmedia/other/uploadify.swf',
                //后台处理的页面
                'uploader': '/newmedia/upload/upload',
                //按钮显示的文字
                'buttonText': '上传图片',
                //显示的高度和宽度，默认 height 30；width 120
                //'height': 15,
                //'width': 80,
                //上传文件的类型  默认为所有文件    'All Files'  ;  '*.*'
                //在浏览窗口底部的文件类型下拉菜单中显示的文本
                //'fileTypeDesc': 'Image Files',
                'fileTypeDesc': 'All Files',
                //允许上传的文件后缀
                'fileTypeExts': '*.gif; *.jpg; *.png;*.txt;*.zip;*.csv;',
                //发送给后台的其他参数通过formData指定
                //'formData': { 'someKey': 'someValue', 'someOtherKey': 1 },
                //上传文件页面中，你想要用来作为文件队列的元素的id, 默认为false  自动生成,  不带#
                //'queueID': 'fileQueue',
                //选择文件后自动上传
                'auto': true,
                //设置为true将允许多文件上传
                'multi': false,
                onUploadProgress: function(file,bytesUploaded,bytesTotal,totalBytesUploaded,totalBytesTotal){
                    $('#pregress').html('总共需要上传'+bytesTotal+'字节，'+'已上传'+totalBytesTotal+'字节');
                },
                onUploadSuccess: function(file,data,respone){
                    $("#context").html(data);
                }
                
            });
        });
    
    </script>
</body>
</html>