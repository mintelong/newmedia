<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>热巢数据分析系统</title>
		
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'index.css')}" type="text/css">
		
		<script type="text/javascript" src="${resource(dir:'js',file:'Chart.min.js') }"></script>
		<script type="text/javascript" src="${resource(dir:'js',file:'default.js') }"></script>
		
	</head>
	<body>
		
		
		
		
		<div class="page-header">
		<h1>热巢数据分析系统</h1>
	</div>
	<ul class="nav nav-tabs" role="tablist" id="myTab">
		<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">文章分析</a></li>
		<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">数据显示</a></li>
	</ul>
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="home">
			<div class="panel panel-default">
				<div class="panel-heading"></div>
				<div class="panel-body">
					<form role="form">
						<div class="row">
							<div class="col-md-4">
								<input type="text" class="form-control" id="title" placeholder="请输入标题..." required>
								<textarea class="form-control" rows="5" id="content" placeholder="请输入文章内容..." required></textarea>
							</div>
							<div class="col-md-8">
								<div class="row">
									<div class="col-md-4">
										<input type="number" class="form-control" id="wechat" required> <label for="wechat">微信分享次数</label>
									</div>
									<div class="col-md-4">
										<input type="number" class="form-control" id="pageviews" required> <label for="pageviews">点击次数</label>
									</div>
									<div class="col-md-4">
										<input type="date" class="form-control" id="pubdate" required> <label for="pubdate">发布日期</label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-8">
								<button type="submit" class="btn btn-default">添加</button>
								<button type="submit" class="btn btn-default">收藏</button>
							</div>
						</div>
					</form>
					<form class="form-inline">
						<label for="title">标题查询</label> <input type="text" class="form-control" id="title" placeholder="请输入标题..."> <label for="pubdate">发布时间查询</label> <input type="date" class="form-control" id="pubdate" placeholder="请输入发布时间...">
						<button type="submit" class="btn btn-default">查询</button>
					</form>
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>标题</th>
								<th>微信分享次数</th>
								<th>点击次数</th>
								<th>发布日期</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>John</td>
								<td>12</td>
								<td>13</td>
								<td>3/16/2015</td>
							</tr>
							<tr>
								<td>2</td>
								<td>John</td>
								<td>12</td>
								<td>13</td>
								<td>3/16/2015</td>
							</tr>
							<tr>
								<td>3</td>
								<td>John</td>
								<td>12</td>
								<td>13</td>
								<td>3/16/2015</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		
		
		
		
		
		
		
		<div role="tabpanel" class="tab-pane" id="profile">
			<div class="panel panel-default">
				<div class="panel-heading">数据概括分析</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							<h3>
								<span class="label label-default">文章（标题）中使用最多的词排行：</span>
							</h3>
							<table class="table">
								<thead>
									<tr>
										<th>#</th>
										<th>词</th>
										<th>数量</th>
										<th>词分类</th>
										<th>感情分类</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>John</td>
										<td>12</td>
									</tr>
									<tr>
										<td>2</td>
										<td>John</td>
										<td>12</td>
									</tr>
									<tr>
										<td>3</td>
										<td>John</td>
										<td>12</td>
									</tr>
								</tbody>
							</table>
							<h3>
								<span class="label label-default">文章（内容）中使用最多的词排行：</span>
							</h3>
							<table class="table">
								<thead>
									<tr>
										<th>#</th>
										<th>词</th>
										<th>数量</th>
										<th>词分类</th>
										<th>感情分类</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>John</td>
										<td>12</td>
									</tr>
									<tr>
										<td>2</td>
										<td>John</td>
										<td>12</td>
									</tr>
									<tr>
										<td>3</td>
										<td>John</td>
										<td>12</td>
									</tr>
								</tbody>
							</table>
							<h3>
								<span class="label label-default">文章中最流行的tag（标签）：</span>
							</h3>
							<table class="table">
								<thead>
									<tr>
										<th>#</th>
										<th>Tag</th>
										<th>文章数量</th>
										<th>分享次数</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>长知识</td>
										<td>12</td>
										<td>13</td>
									</tr>
									<tr>
										<td>2</td>
										<td>环球</td>
										<td>12</td>
										<td>13</td>
									</tr>
									<tr>
										<td>3</td>
										<td>动物</td>
										<td>12</td>
										<td>13</td>
									</tr>
								</tbody>
							</table>
							<h3>
								<span class="label label-default">文章中最流行情感分类：</span>
							</h3>
							<table class="table">
								<thead>
									<tr>
										<th>#</th>
										<th>情感分类</th>
										<th>文章数量</th>
										<th>分享次数</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>伤心</td>
										<td>1</td>
										<td>1</td>
									</tr>
									<tr>
										<td>2</td>
										<td>难过</td>
										<td>2</td>
										<td>13</td>
									</tr>
									<tr>
										<td>3</td>
										<td>高兴</td>
										<td>12</td>
										<td>13</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-6">
							<h3>
								<span class="label label-default">标题中的数字和文章总数分析表：</span>
							</h3>
							<canvas id="myChart" width="400" height="400"></canvas>
							<h3>
								<span class="label label-default">标题中的数字和微信分享量分析表：</span>
							</h3>
							<canvas id="myChart2" width="400" height="400"></canvas>
							<h3>
								<span class="label label-default">文章字数和微信分享量分析表：</span>
							</h3>
							<canvas id="myChart3" width="400" height="400"></canvas>
							<h3>
								<span class="label label-default">文章字数和微信阅读量分析表：</span>
							</h3>
							<canvas id="myChart4" width="400" height="400"></canvas>
							<h3>
								<span class="label label-default">微信阅读量和微信分享量分析表：</span>
							</h3>
							<canvas id="myChart5" width="400" height="400"></canvas>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		
		
		
		
		
		
		
		
	</body>
</html>
