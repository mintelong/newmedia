<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>热巢网络科技有限公司</title>
        <link rel="stylesheet" href="${resource(file: 'css/bootstrap.min.css')}" type="text/css"/>
		<link rel="stylesheet" href="${resource(file: 'css/default.css')}" type="text/css"/>
        
        <script src="${resource(dir: 'js', file: 'jquery-1.10.2.js')}" type="text/javascript"></script>
        
        <g:layoutHead/>
    </head>
	<body>
		<header class="navbar">
            <div class="container">
            
                <ul class="nav navbar-nav navbar-right">
                    <div class="top-active active-1 position-1"></div>
                    <li class="active"><a href="#">首页<div class="wrap"></div><span class="subtitle">INDEX</span></a></li>
                    <li class="active"><a href="#">联系我们<div class="wrap"></div><span class="subtitle">CONTACT</span></a></li>                                                       
                </ul>
                
            </div>
        </header>
		<g:layoutBody/>
		
		<footer>
            <!--foot-->
            <div class="foot">
                <div class="container">
                    <h2>
                    </h2>
                    <div>
                        <p class="float_l">©Rechaos.com.  all rights reserved. About | Contact</p>
                        <p class="float_r">App version: <g:meta name="app.version"></g:meta>. build: <g:meta name="grails.war.time"></g:meta><br/>©Rechaos.com.  all rights reserved. <a href="###">About</a> | <a href="###">Contact</a></p>
                    </div>
                </div>
            </div>
        </footer>
        
        
        
        <!-- 默认显示登录div -->
        <script type="text/javascript">
        </script>
</html>
