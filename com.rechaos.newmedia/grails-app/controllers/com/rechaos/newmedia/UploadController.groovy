package com.rechaos.newmedia

import com.rechaos.AnalysisPlugin.ContextAnalysis
import com.rechaos.AnalysisPlugin.ViralPrabability
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile



class UploadController {

    def index() {
		
		
		println "=============="
		
		
		render view : "index"
	}
	def upload(){
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request
		//文件对象
		
		CommonsMultipartFile file = (CommonsMultipartFile) multipartRequest.getFile("Filedata")
		
		String str = new String(file.getBytes() ,"UTF-8")

		println str
		ViralPrabability t = new ViralPrabability(new ContextAnalysis(str));
		t.getViralPrabability();
		render "得分："+t.getViralPrabability()+"<br>"+str;
	}
}
