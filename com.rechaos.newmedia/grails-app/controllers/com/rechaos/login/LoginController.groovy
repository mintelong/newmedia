package com.rechaos.login

import com.rechaos.analysis.*;
import com.rechaos.analysis.ArticleNumRpt
import com.rechaos.analysis.Wordcount
import com.rechaos.fudannlp.PartsOfSpeechTag
import com.rechaos.service.*;

import java.text.SimpleDateFormat
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry

class LoginController {

	def springSecurityService

	/**
	 * 首页
	 * **/
	def index() {

		render view:"index",model:[]
	}

	/**
	 * 文章分析
	 * **/
	def analysis() {
	//	PartsOfSpeechTag.tags()
		params.max = 6
		List<Article> articles = Article.list(params)
		int totalCount = Article.count()

		render view:"analysis",model:[articles:articles,totalCount:totalCount]
	}
	/**
	 *标题目录分词、极性
	 */
	def runAnalysis(){
		println "------------"
		Map<String, Integer> numCount =  NumCountAnalysis.getNumCount()
		Map<String, Integer> numTView =  NumCountAnalysis.getTViewCount()
		Map<String, Integer> numTShare = NumCountAnalysis.getTShareCount()
		
		
		Map<String, Integer> numCView  = NumCountAnalysis.getCViewCount()
		Map<String, Integer> numCShare = NumCountAnalysis.getCShareCount()
		Map<String, Integer> numVShare = NumCountAnalysis.getVShareCount()
		println "------------"
		
		NumCountAnalysis.ConvertAndSave(NumCountAnalysis.Type_Article_Num, numCount);
		NumCountAnalysis.ConvertAndSave(NumCountAnalysis.Type_TView_Count, numTView);
		NumCountAnalysis.ConvertAndSave(NumCountAnalysis.Type_TShare_Count, numTShare);
		
		
		NumCountAnalysis.ConvertAndSave(NumCountAnalysis.Type_CView_Count, numCView);
		NumCountAnalysis.ConvertAndSave(NumCountAnalysis.Type_CShare_Count, numCShare);
		NumCountAnalysis.ConvertAndSave(NumCountAnalysis.Type_View_Share, numVShare);
		
		DataAnalysis da = new DataAnalysis()
		String rootPath = request.getSession().getServletContext().getRealPath("/")
		EmtionAnalysis.getArticleEmtion(rootPath)
		
		
		da.title(rootPath,"title")
		da.title(rootPath,"content")
		redirect(action: "show")
	}
	/**
	 * 数据显示
	 * **/
	def show() {
		String name = params.name
		String val = params.val
		String namecix = params.namecix
		String valcix = params.valcix
		params.max = 10
		println params
		//标签分组统计
		//List<Map.Entry<String,List<Article>>> listl = new ArrayList<Map.Entry<String,List<Article>>>();
		
		Map map1 = Article.findAll().groupBy{
			elem->elem.type1
		}
	//	List<Map.Entry<String,List<Article>>> listl = new ArrayList<Map.Entry<String,List<Article>>>(map1.entrySet());
		List<Map.Entry<String,List<Article>>> list1 = new ArrayList<Map.Entry<String,List<Article>>>(map1.entrySet());
		Map map2 = Article.findAll().groupBy{
			elem->elem.type2
		}
		List<Map.Entry<String,List<Article>>> list2 = new ArrayList<Map.Entry<String,List<Article>>>(map2.entrySet());
		Map map3 = Article.findAll().groupBy{
			elem->elem.type3
		}
		List<Map.Entry<String,List<Article>>> list3 = new ArrayList<Map.Entry<String,List<Article>>>(map3.entrySet());
		Map map4 = Article.findAll().groupBy{
			elem->elem.type4
		}
		List<Map.Entry<String,List<Article>>> list4 = new ArrayList<Map.Entry<String,List<Article>>>(map4.entrySet());
		Map map5 = Article.findAll().groupBy{
			elem->elem.type5
		}
		List<Map.Entry<String,List<Article>>> list5 = new ArrayList<Map.Entry<String,List<Article>>>(map5.entrySet());
		Map map6 = Article.findAll().groupBy{
			elem->elem.type6
		}
		List<Map.Entry<String,List<Article>>> list6 = new ArrayList<Map.Entry<String,List<Article>>>(map6.entrySet());
		Map map7 = Article.findAll().groupBy{
			elem->elem.type7
		}
		List<Map.Entry<String,List<Article>>> list7 = new ArrayList<Map.Entry<String,List<Article>>>(map7.entrySet());
		Map map8 = Article.findAll().groupBy{
			elem->elem.type8
		}
		List<Map.Entry<String,List<Article>>> list8 = new ArrayList<Map.Entry<String,List<Article>>>(map8.entrySet());
		Map map9 = Article.findAll().groupBy{
			elem->elem.type9
		}
		List<Map.Entry<String,List<Article>>> list9 = new ArrayList<Map.Entry<String,List<Article>>>(map9.entrySet());
		Map map10 = Article.findAll().groupBy{
			elem->elem.type10
		}
		List<Map.Entry<String,List<Article>>> list10 = new ArrayList<Map.Entry<String,List<Article>>>(map10.entrySet());
		Map map11 = Article.findAll().groupBy{
			elem->elem.type11
		}
		List<Map.Entry<String,List<Article>>> list11 = new ArrayList<Map.Entry<String,List<Article>>>(map11.entrySet());
		Map map12 = Article.findAll().groupBy{
			elem->elem.type12
		}
		List<Map.Entry<String,List<Article>>> list12 = new ArrayList<Map.Entry<String,List<Article>>>(map12.entrySet());
		Map map13 = Article.findAll().groupBy{
			elem->elem.type13
		}
		List<Map.Entry<String,List<Article>>> list13 = new ArrayList<Map.Entry<String,List<Article>>>(map13.entrySet());
		Map map14 = Article.findAll().groupBy{
			elem->elem.type14
		}
		List<Map.Entry<String,List<Article>>> list14 = new ArrayList<Map.Entry<String,List<Article>>>(map14.entrySet());
		Map map15 = Article.findAll().groupBy{
			elem->elem.type15
		}
		List<Map.Entry<String,List<Article>>> list15 = new ArrayList<Map.Entry<String,List<Article>>>(map15.entrySet());
		Map map16 = Article.findAll().groupBy{
			elem->elem.type16
		}
		List<Map.Entry<String,List<Article>>> list16 = new ArrayList<Map.Entry<String,List<Article>>>(map16.entrySet());
		Map map17 = Article.findAll().groupBy{
			elem->elem.type17
		}
		List<Map.Entry<String,List<Article>>> list17 = new ArrayList<Map.Entry<String,List<Article>>>(map17.entrySet());
		Map map18 = Article.findAll().groupBy{
			elem->elem.type18
		}
		List<Map.Entry<String,List<Article>>> list18 = new ArrayList<Map.Entry<String,List<Article>>>(map18.entrySet());
		Map map19 = Article.findAll().groupBy{
			elem->elem.type19
		}
		List<Map.Entry<String,List<Article>>> list19 = new ArrayList<Map.Entry<String,List<Article>>>(map19.entrySet());
		Map map20 = Article.findAll().groupBy{
			elem->elem.type20
		}
		List<Map.Entry<String,List<Article>>> list20 = new ArrayList<Map.Entry<String,List<Article>>>(map20.entrySet());
		Map map21 = Article.findAll().groupBy{
			elem->elem.type21
		}
		List<Map.Entry<String,List<Article>>> list21 = new ArrayList<Map.Entry<String,List<Article>>>(map21.entrySet());
		
		//然后通过比较器来实现排序
		Collections.sort(list1,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		List<String> types = new ArrayList<String>()
		for(Map.Entry<String,List<Article>> mapping:list1){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		
		//然后通过比较器来实现排序
		Collections.sort(list2,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list2){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		
		//然后通过比较器来实现排序
		Collections.sort(list3,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list3){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		
		//然后通过比较器来实现排序
		Collections.sort(list4,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list4){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		
		//然后通过比较器来实现排序
		Collections.sort(list5,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list5){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		
		//然后通过比较器来实现排序
		Collections.sort(list6,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list6){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		
		//然后通过比较器来实现排序
		Collections.sort(list7,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list7){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		
		//然后通过比较器来实现排序
		Collections.sort(list8,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list8){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		
		//然后通过比较器来实现排序
		Collections.sort(list9,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list9){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		
		//然后通过比较器来实现排序
		Collections.sort(list10,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list10){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		
		//然后通过比较器来实现排序
		Collections.sort(list11,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list11){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		
		//然后通过比较器来实现排序
		Collections.sort(list12,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list12){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		
		//然后通过比较器来实现排序
		Collections.sort(list13,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list13){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		
		//然后通过比较器来实现排序
		Collections.sort(list14,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list14){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		
		//然后通过比较器来实现排序
		Collections.sort(list15,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list15){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		
		//然后通过比较器来实现排序
		Collections.sort(list16,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list16){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		
		//然后通过比较器来实现排序
		Collections.sort(list17,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list17){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		
		//然后通过比较器来实现排序
		Collections.sort(list18,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list18){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		
		//然后通过比较器来实现排序
		Collections.sort(list19,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list19){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		//然后通过比较器来实现排序
		Collections.sort(list20,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list20){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		//然后通过比较器来实现排序
		Collections.sort(list21,new Comparator<Map.Entry<String,List<Article>>>() {
			public int compare( Map.Entry<String,List<Article>> o1,  Map.Entry<String,List<Article>> o2) {
				return (o2.getValue().size()).compareTo(o1.getValue().size());
			}
			
		});
		for(Map.Entry<String,List<Article>> mapping:list21){
			System.out.println(mapping.getKey()+":"+mapping.getValue().size())
			int k = mapping.getValue().sum{
				elem -> elem.wechats
			}
			if(mapping.getKey()!=null){
				types.add(mapping.getKey()+"_"+mapping.getValue().size()+"_"+k)
			}
		}
		
		
		//=============================================================================
		
		List<Wordcount> titles
		List<Wordcount> contents
		String title1=null
		String content1=null
		String titilecix=null
		String contentcix=null
		if(name!=null){
			if(name.equals("title")){
				titles = Wordcount.findAllByCateAndPolarity("title",val,[max: 10, sort: "count", order: "desc",offset:params.offset])
				contents = Wordcount.findAllByCate("content",[max: 10, sort: "count", order: "desc",offset:params.offset])
				title1 = val
			}
			if(name.equals("content")){
				titles = Wordcount.findAllByCate("title",[max: 10, sort: "count", order: "desc",offset:params.offset])
				contents = Wordcount.findAllByCateAndPolarity("content",val,[max: 10, sort: "count", order: "desc",offset:params.offset])
				content1 = val
			}
		}else if(namecix!=null){
			if(namecix.equals("titlecix")){
				titles = Wordcount.findAllByCateAndCix("title",valcix,[max: 10, sort: "count", order: "desc",offset:params.offset])
				contents = Wordcount.findAllByCate("content",[max: 10, sort: "count", order: "desc",offset:params.offset])
				titilecix = valcix
			}
			if(namecix.equals("contentcix")){
				titles = Wordcount.findAllByCate("title",[max: 10, sort: "count", order: "desc",offset:params.offset])
				contents = Wordcount.findAllByCateAndCix("content",valcix,[max: 10, sort: "count", order: "desc",offset:params.offset])
				contentcix = valcix
			}
		}else{
			titles = Wordcount.findAllByCate("title",[max: 10, sort: "count", order: "desc",offset:params.offset])
			contents = Wordcount.findAllByCate("content",[max: 10, sort: "count", order: "desc",offset:params.offset])
		}
		int titleCount = Wordcount.findAllByCate("title").size()
		int contentCount = Wordcount.findAllByCate("content").size()
		int tagCount     = types.size();
		println "------------"
//		Map<String, Integer> numCount =  NumCountAnalysis.getNumCount()
//		Map<String, Integer> numTView =  NumCountAnalysis.getTViewCount()
//		Map<String, Integer> numTShare = NumCountAnalysis.getTShareCount()
//		Map<String, Integer> numCView  = NumCountAnalysis.getCViewCount()
//		Map<String, Integer> numCShare = NumCountAnalysis.getCShareCount()
		def numCount =  ArticleNumRpt.findAllByReportName(NumCountAnalysis.Type_Article_Num).size() > 0?
		ArticleNumRpt.findAllByReportName(NumCountAnalysis.Type_Article_Num).get(0).getReport(): null
		def numTView =  ArticleNumRpt.findAllByReportName(NumCountAnalysis.Type_TView_Count).size() > 0?
		ArticleNumRpt.findAllByReportName(NumCountAnalysis.Type_TView_Count).get(0).getReport():null
		def numTShare = ArticleNumRpt.findAllByReportName(NumCountAnalysis.Type_TShare_Count).size() > 0?
		ArticleNumRpt.findAllByReportName(NumCountAnalysis.Type_TShare_Count).get(0).getReport():null
		
		//需要特殊处理
		def numCView  = ArticleNumRpt.findAllByReportName(NumCountAnalysis.Type_CView_Count).size() > 0?
		ArticleNumRpt.findAllByReportName(NumCountAnalysis.Type_CView_Count).get(0).getReport():null
		def numCShare = ArticleNumRpt.findAllByReportName(NumCountAnalysis.Type_CShare_Count).size() > 0?
		ArticleNumRpt.findAllByReportName(NumCountAnalysis.Type_CShare_Count).get(0).getReport():null
		def numVShare = ArticleNumRpt.findAllByReportName(NumCountAnalysis.Type_View_Share).size() > 0?
		ArticleNumRpt.findAllByReportName(NumCountAnalysis.Type_View_Share).get(0).getReport():null
		println "------------"
		
		//按步长进行合并
		int step = 100;// default step = 100
		numCView = NumCountAnalysis.combine(numCView)
		numCShare = NumCountAnalysis.combine(numCShare)
		numVShare = NumCountAnalysis.combine(numVShare)
		
		println "=="+numCount
		//将map 按照 key 排序
		if(numCount!=null){
			numCount =  new TreeMap(numCount)
		}
		if(numTView!=null){
			numTView =  new TreeMap(numTView)
		}
		if(numTShare!=null){
			numTShare =  new TreeMap(numTShare)
		}
		if(numCView!=null){
			numCView =  new TreeMap(numCView)
		}
		if(numCShare!=null){
			numCShare =  new TreeMap(numCShare)
		}
		if(numVShare!=null){
			numVShare =  new TreeMap(numVShare)
		}
		
		def emtions = ArticleEmtion.findAll()
		
		
//		render view:"show",model:["numCount":numCount.report, "numTView":numTView.report, "numTShare":numTShare.report, "numCView":numCView.report, "numCShare":numCShare.report,
		render view:"show",model:["numCount":numCount, "numTView":numTView, "numTShare":numTShare, "numCView":numCView, "numCShare":numCShare,"numVShare":numVShare,
		"tagCount":tagCount	,"emtions":emtions,"types":types,"titles":titles,"contents":contents,"titleCount":titleCount,"contentCount":contentCount,"title1":title1,"content1":content1,"titilecix":titilecix,"contentcix":contentcix,"step":step,]
	}
	
	/**
	 * 添加文章
	 * **/
	def addArticle() {
		println params
		String title = params.title
		String content = params.content
		String wechats = params.wechats
		String pageviews = params.pageviews
		String picnum = params.picnum
		String pubdate = params.pubdate
		
		String type1 = null
		String type2 = null
		String type3 = null
		String type4 = null
		String type5 = null
		String type6 = null
		String type7 = null
		String type8 = null
		String type9 = null
		String type10 = null
		String type11 = null
		String type12 = null
		String type13 = null
		String type14 = null
		String type15 = null
		String type16 = null
		String type17 = null
		String type18 = null
		String type19 = null
		String type20 = null
		String type21 = null
		
		String type = null
		
		int k = 0
		println params."type1".equals("on")
		
		for(int i=1;i<=19;i++){
			String temp = "type"+i
			Map m = new HashMap()
			m.put("tm", temp)
			if(params.(m.get("tm")).equals("on")){				
				k = i								
				switch(k){
					case 1:type = "明星";break
					case 2:type = "爱情";break
					case 3:type = "电视";break
					case 4:type = "怀旧";break
					case 5:type = "音乐";break
					case 6:type = "科技";break
					case 7:type = "时事";break
					case 8:type = "运动";break
					case 9:type = "动物";break				
					case 10:type = "DIY";break
					case 11:type = "旅游";break
					case 12:type = "健康";break
					case 13:type = "金钱";break
					case 14:type = "电影";break
					case 15:type = "知识";break
					case 16:type = "趣事";break
					case 17:type = "启迪";break
					case 18:type = "视觉";break
					case 19:type = "时尚";break
					case 20:type = "美食";break
					case 21:type = "有料";break
				}
				
				if(k==1){
					type1 = type
				}
				if(k==2){
					type2 = type
				}
				if(k==3){
					type3 = type
				}
				if(k==4){
					type4 = type
				}
				if(k==5){
					type5 = type
				}
				if(k==6){
					type6 = type
				}
				if(k==7){
					type7 = type
				}
				if(k==8){
					type8 = type
				}
				if(k==9){
					type9 = type
				}
				if(k==10){
					type10 = type
				}
				if(k==11){
					type11 = type
				}
				if(k==12){
					type12 = type
				}
				if(k==13){
					type13 = type
				}
				if(k==14){
					type14 = type
				}
				if(k==15){
					type15 = type
				}
				if(k==16){
					type16 = type
				}
				if(k==17){
					type17 = type
				}
				if(k==18){
					type18 = type
				}
				if(k==19){
					type19 = type
				}
				if(k==20){
					type20 = type
				}
				if(k==21){
					type21 = type
				}
			}
		}
		
		println type
		
		SimpleDateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy")
		
		try{
			Article articleInstance = new Article()
			articleInstance.title = title
			articleInstance.content = content
			articleInstance.wechats = Integer.valueOf(wechats)
			articleInstance.pageviews = Integer.valueOf(pageviews)
			articleInstance.picnum = Integer.valueOf(picnum)
			articleInstance.pubdate = dateformat.parse(pubdate)
			articleInstance.type1 = type1
			articleInstance.type2 = type2
			articleInstance.type3 = type3
			articleInstance.type4 = type4
			articleInstance.type5 = type5
			articleInstance.type6 = type6
			articleInstance.type7 = type7
			articleInstance.type8 = type8
			articleInstance.type9 = type9
			articleInstance.type10 = type10
			articleInstance.type11 = type11
			articleInstance.type12 = type12
			articleInstance.type13 = type13
			articleInstance.type14 = type14
			articleInstance.type15 = type15
			articleInstance.type16 = type16
			articleInstance.type17 = type17
			articleInstance.type18 = type18
			articleInstance.type19 = type19
			articleInstance.type20 = type20
			articleInstance.type21 = type21
			articleInstance.save flush:true
		}catch(Exception e){
			println "addArticle catch exception."+e.getMessage()
		}
		
		redirect(action:"analysis")
		//render view:"analysis",model:[]
	}
	
	/**
	 * 按照标题查询
	 * **/
	def search() {
		
		String title = params.title
		String pubdate = params.pubdate
		
		params.max = 6
		
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd")
		
		def DTCreate = Article.createCriteria()
		
		def filterClosure = {
			if (title!=null&&title!="") {
				like("title","%"+title+"%")
			}
			if (pubdate!=null&&pubdate!="") {
				eq("pubdate",dateformat.parse(pubdate))
			}
		}
		
		List<Article> articles = DTCreate.list(params,filterClosure)
		int totalCount = DTCreate.count(filterClosure)
		
		render view:"analysis",model:[articles:articles,totalCount:totalCount]
	}
	
	/**
	 * 登陆认证
	 * **/
	def auth(){
		params.max = 6
		List<Article> articles = Article.list()
		int totalCount = Article.count()
		
		render view:"analysis",model:[articles:articles,totalCount:totalCount]
	}
	
	
	/**
	 * 删除
	 * **/
	def delete() {
		
		String articleid = params.articleid
		
		Article article = Article.findById(Long.valueOf(articleid))
		
		article.delete flush:true
		
		redirect(action:"analysis")
	}
	
}
