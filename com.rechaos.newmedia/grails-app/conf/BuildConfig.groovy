grails.servlet.version = "3.0" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.work.dir = "target/work"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
//grails.project.war.file = "target/${appName}-${appVersion}.war"

grails.project.fork = [
    // configure settings for compilation JVM, note that if you alter the Groovy version forked compilation is required
    //  compile: [maxMemory: 256, minMemory: 64, debug: false, maxPerm: 256, daemon:true],

    // configure settings for the test-app JVM, uses the daemon by default
    test: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, daemon:true],
    // configure settings for the run-app JVM
    run: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, forkReserve:false],
    // configure settings for the run-war JVM
    war: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, forkReserve:false],
    // configure settings for the Console UI JVM
    console: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256]
]

grails.project.dependency.resolver = "maven" // or ivy
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // specify dependency exclusions here; for example, uncomment this to disable ehcache:
        // excludes 'ehcache'
    }
    log "error" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve
    legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

    repositories {
        inherits true // Whether to inherit repository definitions from plugins

        grailsPlugins()
        grailsHome()
        mavenLocal()
        grailsCentral()
        mavenCentral()
		
		//===================
		mavenRepo "http://repo.grails.org/grails/core"
		mavenRepo "http://repo.spring.io/milestone/"
		//===================
		
        // uncomment these (or add new ones) to enable remote dependency resolution from public Maven repositories
        //mavenRepo "http://repository.codehaus.org"
        //mavenRepo "http://download.java.net/maven/2/"
        //mavenRepo "http://repository.jboss.com/maven2/"
    }

    dependencies {
//		compile 'org.apache.httpcomponents:httpclient:4.3.1'
		
		//===================
		compile 'net.sf.ehcache:ehcache-core:2.4.8'
//		compile 'commons-beanutils:commons-beanutils:1.8.3'
		//===================
		
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes e.g.
        // runtime 'mysql:mysql-connector-java:5.1.24'
		provided 'org.spockframework:spock-grails-support:0.5-groovy-1.7', {
			exclude 'groovy-all'
		}
//		runtime 'org.apache.activemq:activemq-all:5.9.0'
    }

    plugins {
		
        // plugins for the build system only
        //build ":tomcat:7.0.47"
		//build ':tomcat:7.0.50.1'
		//build ':tomcat:7.0.54'
		build ':tomcat:7.0.52.1'

		//compile ":tomcat8:8.0.5"
        // plugins for the compile step
        //compile ":scaffolding:2.0.0"
//		compile ":scaffolding:2.1.2"
		
        //compile ':cache:1.1.1'
//		compile ':cache:1.1.7'
		
		//compile ':twitter-bootstrap:2.3.2.2'
		compile ':twitter-bootstrap:3.2.0.2'
		//export ���
		//compile ':export:1.5'
//		compile ':export:1.6'
		
		//security
		
		//compile ":spring-security-core:1.2.7.3"
		//compile ":spring-security-cas:1.0.5"
		compile ":spring-security-core:2.0-RC4"
//		compile ":spring-security-cas:2.0-RC1"
		
//		compile ":quartz:1.0.1"
		//===================
		compile ':asset-pipeline:1.8.11'
		//===================
		
//		runtime ":webxml:1.4.1"
		
//		compile ":routing-jms:1.2.0"
        // plugins needed at runtime but not for compilation
        //runtime ":hibernate:3.6.10.1" // or ":hibernate4:4.1.11.1"
        //runtime ":database-migration:1.3.5"
        //runtime ":jquery:1.10.2"
        //runtime ":resources:1.2"
		//runtime ":twitter-bootstrap:3.2.0.2" // current: 3.2.0.2
		
		runtime ":jquery:1.11.1"
		
		//==============================================================================
        // Uncomment these (or add new ones) to enable additional resources capabilities
		//runtime ":resources:1.2.8"
        //runtime ":zipped-resources:1.0"
        //runtime ":cached-resources:1.0"
        //runtime ":yui-minify-resources:0.1.5"
		//compile ":lesscss-resources:1.3.3"
		//compile ":lesscss-resources:1.3.0.3"
		//==============================================================================
		
		
		compile ":mongodb:1.3.1"
//		compile ":functional-spock:0.6"
		
//		compile ":cookie-session:2.0.13"
//		compile ":csv:0.3.1"
//		compile ":joda-time:1.5"
		//compile ":excel-import:1.0.0"
		
    }
}
grails.war.resources = { stagingDir, args ->
	def df1 = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
	String timeflag =df1.format(new Date())
	File config = new File("${stagingDir}/WEB-INF/classes/application.properties")
	FileOutputStream fs = new FileOutputStream(config,true)
	String line = "grails.war.time="+timeflag+"\n"
	fs.write(line.getBytes())
	fs.close()
}
