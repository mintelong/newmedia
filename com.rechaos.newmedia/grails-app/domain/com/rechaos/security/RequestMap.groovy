package com.rechaos.security

import org.bson.types.ObjectId;
import org.springframework.http.HttpMethod

class RequestMap {
	ObjectId id
	String url
	String configAttribute
	HttpMethod httpMethod

	static mapping = {
		cache true
	}

	static constraints = {
		url blank: false, unique: 'httpMethod'
		configAttribute blank: false
		httpMethod nullable: true
	}
}
