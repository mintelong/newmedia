package com.rechaos.security

import org.bson.types.ObjectId;

class Authority {
	ObjectId id
	String authority

	static mapping = {
		cache true
	}

	static constraints = {
		authority blank: false, unique: true
	}
}
