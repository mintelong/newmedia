package com.rechaos.analysis

class ArticleNumRpt {
	String  reportName;
	Map<Integer, Integer> report;
    static constraints = {
		reportName nullable:false,unique:true
    }
}
