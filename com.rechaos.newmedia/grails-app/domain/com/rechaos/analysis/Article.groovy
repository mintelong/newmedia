package com.rechaos.analysis

import org.grails.databinding.BindingFormat

class Article {
	
	String title //文章标题
	String content //文章内容
	
	int wechats //微信分享次数
	int pageviews //点击次数
	int picnum //图片总数
	
	@BindingFormat('yyyy-MM-dd')
	Date pubdate  //发布日期
	
	String reserve //预留字段
	
	String tAnalysisStats;
	String cAnalysisStats;
	String eAnalysisStats;
	
	String type1 //明星
	String type2 //爱情
	String type3 //
	String type4 //
	String type5 //
	String type6 //
	String type7 //
	String type8 //
	String type9 //
	String type10 //
	String type11 //
	String type12 //
	String type13 //
	String type14 //
	String type15 //
	String type16 //
	String type17 //
	String type18 //
	String type19 //
	
	String type20 //
	String type21 //
	
    static constraints = {
		title nullable:false
		content nullable:false
		
		wechats nullable:true
		pageviews nullable:true
		picnum nullable:true
		pubdate nullable:true
		reserve nullable:true
		
		tAnalysisStats nullable:true
		cAnalysisStats nullable:true
		eAnalysisStats nullable:true
		
		type1 nullable:true
		type2 nullable:true
		type3 nullable:true
		type4 nullable:true
		type5 nullable:true
		type6 nullable:true
		type7 nullable:true
		type8 nullable:true
		type9 nullable:true
		type10 nullable:true
		type11 nullable:true
		type12 nullable:true
		type13 nullable:true
		type14 nullable:true
		type15 nullable:true
		type16 nullable:true
		type17 nullable:true
		type18 nullable:true
		type19 nullable:true
		type20 nullable:true
		type21 nullable:true
    }
}
